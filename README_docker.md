#### Kept for a later rewrite/restructure

# Docker instructions

To ease the installtion of required softwares for the Hylleraas Platform, we are exploring the possbility of using precompiled images using Docker. The effort for the user, and for developers, is then shifted from installing multiple softwares, often with conflicing compiler requirements, to installation of Docker instead (and  automated downloads of precomiled images). Installation instructions for Docker can be found [here](https://docs.docker.com/get-docker/).

With Docker installed you can download a precompiled container image containing Dalton and LSDALTON by executing
```
docker pull audunsh/dproject
```
You can have several images locally, also different versions of the same software (`docker images` give a list of your local images). You can run lsdalton from the ´audunsh/dproject´ container on your terminal by executing, e.g.
```
docker run -v /Users/simensr/Docker/runs:/temp/runs/ audunsh/dproject /bin/bash -c "cd /temp/runs; /temp/lsdalton/build/lsdalton LSDALTON_b3lyp_water"
```
This command specifies to run the docker image `audunsh/dproject` with
- the `-v` option mounting the host directory `/Users/simensr/Docker/runs` to the container directory `/temp/runs/`. This allows input files from the host to be accessible within the container, and for output generated within the container to be accessible on the host. The input files, `LSDALTON_b3lyp_water.dal` and `LSDALTON_b3lyp_water.mol`, are copied to `/Users/simensr/Docker/runs` prior to execution. (Within the platform such operations will be hidden from the user.)
- the `/bin/bash -c` allowing running several commands, separated by semicolons
- two commands executed in the container: first we enter the mounted directory `/temp/runs/`, and then we run lsdalton using the input files there
- the output files (`LSDALTON_b3lyp_water.out` and `LSDALTON_b3lyp_water.tar.gz`) will now exist on the host directory `/Users/simensr/Docker/runs`



## Install Docker and Docker Hub

## Create Docker image

A docker *image* is a self-contained environment containing applications and dependencies. Images are instantiated and executed as *containers*. The simplest way to create a new image is to base it on pre-existing images, for instance on docker hub.

A local image of an existing Ubuntu-image with Anaconda/python3 installed may be created from the command line by running:
```
docker pull continuumio/anaconda3
```

The container may thereafter be executed with
```
docker run -i -t continuumio/anaconda3 /bin/bash
```
where `-i -t` enables an interactive bash.

Additional software may now be installed in the `continuumio/anaconda3` manually, or by the `conda` or `apt` package managers.

**Note:** if the session is ended by the `exit` command, changes to the container will not affect the image, and any changes will be lost. In order to save changes, a new image must be created from outside the container. First run:
```
docker ps -a
```
in order to list all local containers. Thereafter, run
```
docker commit [CONTAINER ID] [NEW IMAGE NAME]
```
To start the new image interactively, run
```
docker run -i -t [NEW IMAGE NAME] /bin/bash
```


## Run Docker
