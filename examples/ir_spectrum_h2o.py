# import matplotlib.pyplot as plt
#
# import hylleraas
#
# water = hylleraas.Molecule('water_ts.xyz')
# lsdalton_dft = hylleraas.Method('dft/b3lyp', program='lsdalton')
# vib = hylleraas.vibrational_frequencies(water, lsdalton_dft, raman=False)
# x, y = hylleraas.gen_spectrum(vib.frequencies,
#                               vib.ir_intensities,
#                               fwhm=15.0,
#                               method='gaussian')
# plt.plot(x, y)
# plt.xlabel('frequency in 1/cm')
# plt.ylabel('intensity')
# plt.savefig('ir_spectrum_h2o.png')
