# type: ignore
import numpy as np
import sympy as sp

import hylleraas as hsp


class Morse:
    """Morse potential interface class."""

    def __init__(self, method, molecule=None):
        """Create interface.

        Parameters
        ----------
        method : :obj:`hylleraas.MethodLike`
            definition of parameter
        molecule : None, optional
            unused Morse, required from HSP

        """
        self.model, self.dmodel = Morse.set_model(method)

    @classmethod
    def symmetrize(cls, molecule):
        """Symmetrize coordinates of diatomics."""
        molecule = hsp.Molecule(molecule)  # better save than sorry
        if molecule.num_atoms != 2:
            raise Exception('Custom potential only defined for diatomtics')
        dist = molecule.distance(0, 1) / 2.0
        molecule.set_coordinates(np.array([[0, 0, dist], [0, 0, -dist]]))
        return molecule

    @classmethod
    def set_model(cls, method):
        """Generate morse potential and force."""
        de, a, r, re = sp.symbols('de a r re')
        morse = de * (1 - sp.exp(-a * (r-re)))**2
        dmorse = sp.diff(morse, r)
        return morse.subs(method), dmorse.subs(method)

    def get_energy(self, molecule):
        """Compute energy."""
        molecule_updated = Morse.symmetrize(molecule)
        r = sp.symbols('r')
        return self.model.subs(r, molecule_updated.distance(0, 1))

    def get_gradient(self, molecule):
        """Compute gradient."""
        molecule_updated = Morse.symmetrize(molecule)
        r = sp.symbols('r')
        grad = np.zeros((2, 3))
        grad[0] = np.array([0, 0, self.dmodel.subs(r, molecule_updated.distance(0, 1))])
        grad[1] = -grad[0]
        return grad


if __name__ == '__main__':

    reval = 1.40
    aval1 = 1.02  # force constant : 2*deval*aval**2
    aval2 = 0.92
    mymol = hsp.Molecule('H 0 0 -1.0 \n H 0 0 1.0')
    mymethod1 = hsp.Method({'a': aval1, 'de': 0.176, 're': reval}, interface=Morse)
    mymethod2 = hsp.Method({'a': aval2, 'de': 0.176, 're': reval}, interface=Morse)
    mymethod3 = mymethod1 + mymethod2.get_gradient

    mygeo = hsp.Compute('geometry_optimization', molecule=Morse.symmetrize(mymol), method=mymethod3)

    mymol.set_coordinates(mygeo.result['final_geometry_bohr'])
    print('converged to minimum :', np.isclose(mymol.distance(0, 1), reval, atol=1e-4))
