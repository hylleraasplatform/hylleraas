# # molecule file format conversion example
# import hylleraas
#
# mol = hylleraas.Molecule('ch3cooh.xyz')
# pdb_string = hylleraas.convert(mol, 'xyz', 'pdb')
# zmat = hylleraas.convert(mol, 'xyz', 'zmat')
#
#
# print('XYZ\n', mol.coordinates, mol.atoms)
# print('PDB from XYZ\n',pdb_string)
# print('ZMAT from XYZ\n',zmat)
#
# mol = hylleraas.Molecule(zmat, filetype='zmat')
# xyz = hylleraas.convert(mol, 'zmat', 'xyz')
# print('XYZ from ZMAT\n', xyz)
# mol = hylleraas.Molecule('ch3cooh.zmat')
# print('XYZ from ZMAT2\n', mol.atoms, mol.coordinates)
