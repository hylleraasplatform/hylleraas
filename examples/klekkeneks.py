# type: ignore
import hylleraas as hsp

water = hsp.Molecule('./examples/water.xyz')
print(water.angle(0, 1, 2))

h2monomer = hsp.Molecule('H 0 0 0 \n H 0 0 1.4')
h2dimer = h2monomer + h2monomer.translate([100, 0, 0])
print(h2dimer.distance(0, 2))

method1 = hsp.Method({'qcmethod': 'HF', 'basis': 'def2-QZVP'}, program='dalton')
method2 = hsp.Method({'qcmethod': 'HF', 'basis': 'def2-TZVP'}, program='lsdalton')
print('\nMETHOD1')
print(method1)
print('\nMETHOD2')
print(method2)
method3 = method1 + method2.get_gradient
print('\nMETHOD3')
print(method3)

# import hylleraas as hsp

dist = 10
monomer = hsp.Molecule(['H', ['H', 1, 1.40]])
dimer = monomer + monomer.translate([dist, 0, 0])
method = hsp.Method({'qcmethod': ['DFT', 'CAMB3LYP'], 'basis': 'def2-TZVPP'}, program='lsdalton')
interaction_energy = method.get_energy(dimer) - 2 * method.get_energy(monomer)
print(interaction_energy)

# import hylleraas as hsp

mol_str = """H 0.0 0.0503996795 -1.0394417252
O -0. 0.0333346304 -0.099999967
H 0.0 0.0162646901 0.8394416921"""
water = hsp.Molecule(mol_str)
dft = hsp.Method({'qcmethod': ['DFT', 'BP86'], 'basis': 'def2-TZVP'}, program='lsdalton')
geo_options = {'optimizer': 'geometric', 'convergence_energy': 1e-7, 'optimization_target': 'transition_state'}
geo = hsp.Compute('geometry_optimization', molecule=water, method=dft, options=geo_options)
water.set_coordinates(geo.result['final_geometry_bohr'])
print(water.angle(0, 1, 2))

# import hylleraas as hsp

my_molecule = hsp.Molecule('./examples/water.xyz')
# smiles_str = hsp.convert(my_molecule, 'xyz', 'smiles', options= {'path_to_xyz2mol' : './myscripts/'})
# [H]O[H]
zmat_list = hsp.convert(my_molecule, 'xyz', 'zmat')
# [['H'], ['O', 1, 0.968573], ['H', 1, 1.52512, 2, 38.066868]]
print(zmat_list)
xyz_list = hsp.convert(zmat_list, 'zmat', 'xyz')
# (['H', 'O', 'H'], array([[0. , 0. , 0. ], [0.968573, 0. , 0. ],
#                         [1.200714, 0.94036 , 0. ]]))
print(xyz_list)
my_molecule1 = hsp.Molecule(zmat_list)
my_molecule2 = hsp.Molecule({'atoms': xyz_list[0], 'coordinates': xyz_list[1]})
if my_molecule1 == my_molecule2:
    print('molecules are identical')
else:
    print('..opts')
    print(my_molecule)
    print(my_molecule2)
# molecules are identical
