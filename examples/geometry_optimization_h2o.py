# import hylleraas
#
# water = hylleraas.Molecule('water.xyz')
# angle0 = water.angle(0, 1, 2)
# lsdalton_dft = hylleraas.Method('dft/b3lyp')
# dalton_dft = hylleraas.Method('dft/b3lyp', program='dalton')
# minimum_native = hylleraas.geometry_optimization(water, lsdalton_dft)
# # needs more testing
# #minimum_geometric = hylleraas.geometry_optimization(water,
# #                                                    lsdalton_dft,
# #                                                    engine="geometric")
#
# print('E(native optimization) = ', minimum_native.energy)
# #print("E(geomeTRIC optimization) = ", minimum_geometric.energy)
#
# #hylleraas.update_molecule(water, minimum_native.final_geometry)
# water.coordiantes = minimum_native.final_geometry
# angle1 = water.angle(0, 1, 2)
# # alternative
# #water.update_geometry(minimum_geometric.final_geometry)
# #angle2 = water.angle(0, 1, 2)
# print('angle(before optimization) = ', angle0)
# print('angle(native optimization) = ', angle1)
# #print("angle(geomeTRIC optimization) = ", angle2)
# hylleraas.view_molecule(water)
