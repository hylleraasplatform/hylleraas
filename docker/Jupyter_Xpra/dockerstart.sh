#!/bin/bash

# set -m

# start xpra server
xpra start :80 --bind-tcp="0.0.0.0:8080" --sharing="yes" --mdns="no" --webcam="no" --no-daemon --start-on-connect="xterm -bg black -fg white" --start="xhost +" &

# avoid output-mess
sleep 5

# start notebook
jupyter notebook --port="8888" --no-browser --ip="0.0.0.0" --allow-root &

# wait for any process to exit
wait -n

# exit with states of process that exited first
exit $?
