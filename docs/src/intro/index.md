# Introduction

The [Hylleraas Software Platform](<https://gitlab.com/hylleraasplatform/hylleraas>)
(HSP) represents an active and evolving project aimed at bolstering the diverse spectrum of research endeavors undertaken within the [Hylleraas Centre](<https://www.mn.uio.no/hylleraas/english/>). With an emphasis on facilitating seamless integration among various software development and computational initiatives, HSP aspires to enable sophisticated multi-scale simulations that extend across a vast range of length and time scales—from single atoms to billions, and from attoseconds to milliseconds.

Beyond its primary mission, HSP endeavors to make a meaningful contribution to the broader scientific community. This is achieved by fostering a sustainable, flexible, and user-friendly platform that will continue to serve users and developers well beyond the lifespan of the Hylleraas Centre itself. Embracing an open-source philosophy, the platform is freely accessible to all interested parties.

We encourage contributions from those who find value in this project. Whether through direct code contributions, suggestions for improvements, or sharing insights that could benefit others, your involvement is warmly welcomed. While formal contribution guidelines are in development, we are ready to share our general guidelines upon request.

![Illustration of the Hylleraas Software Platform](hsp3.pdf)
