# Modules

We here list modules that are integral parts of HSP, and are automatically installed together with the platform by pip.

## Geometry Optimization

[HyGO](<https://geometryoptimizer.readthedocs.io/en/latest/index.html>) is a standalone module for geometry optimization with support for Optking and geomeTRIC.

## Active Learning

[HyAL](<https://gitlab.com/hylleraasplatform/hyal>) is a module for active learning, which automates the process of generating data and training machine-learning methods, like machine-learning potentials (MLPs) use for example for time evolution.

> *ToDO:* include all relevant modules here, and clean up git an move unused modules to a backup repository
