:html_theme.sidebar_secondary.remove: true

***************************
Hylleraas Software Platform
***************************

**Date**: |today| **Version**: 0.0.1-alpha

**Download documentation**: https://hylleraas.readthedocs.io/

This is the documentation for the open-source **Hylleraas Software Platform**
(HSP) project. The **HSP** is a Python software platform for running complex
chemical simulation workflows easily and efficiently.

.. grid:: 1
    :margin: 0 3 0 0

    .. grid-item-card::
        :text-align: center
        :margin: auto
        :link: installation/installation.html
        :columns: 8

        .. centered::
            :material-regular:`power_settings_new;45px` **Installation**

        ++++++++++

        Instructions on how to install the Hylleraas Software Platform and
        its dependencies on your system.

.. grid:: 3
    :gutter: 3

    .. grid-item-card::
        :link: user_guide/user_guide.html

        .. centered::
            :material-regular:`menu_book;75px`

        .. centered:: **User guide**

        ++++++++++

        Navigate through the comprehensive user guide section for detailed
        instructions and tips on using the Hylleraas Software Platform.

    .. grid-item-card::
        :link: developers_guide/developers_guide.html

        .. centered::
            :material-regular:`terminal;75px`

        .. centered:: **Developer guide**

        ++++++++++

        Explore the developer guide for in-depth technical documentation and
        resources tailored to assist developers in integrating and extending
        the HSP.

    .. grid-item-card::
        :link: api/index.html

        .. centered::
            :material-regular:`webhook;75px`

        .. centered:: **API reference**

        ++++++++++

        The API reference contains a detailed description of
        the HSP modules, classes, and functions, as well as their
        parameters and returned objects.




..
    .. grid-item-card::
        :link: installation/installation.html

        .. centered::
            :material-regular:`power_settings_new;75px`

        .. centered:: **Installation**

        ++++++++++

        Follow the step-by-step installation guide to seamlessly set up the
        Hylleraas Software Platform on your system.
