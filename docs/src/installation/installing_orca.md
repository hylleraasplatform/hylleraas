(installing_orca)=
# Installing Orca for the Hylleraas Software Platform
Please see the [Orca installation instructions](https://www.orcasoftware.de/tutorials_orca/first_steps/install.html) for information on how to compile Orca.
