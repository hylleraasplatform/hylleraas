(installing_xtb)=
# Installing xTB for the Hylleraas Software Platform
Please see the [xTB installation instructions](https://xtb-docs.readthedocs.io/en/latest/setup.html) for information on how to compile xTB.
