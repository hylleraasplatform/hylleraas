(installing_lammps)=
# Installing LAMMPS for the Hylleraas Software Platform
Please see the [LAMMPS installation instructions](https://docs.lammps.org/Install.html) for information on how to compile LAMMPS.
