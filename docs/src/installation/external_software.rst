.. _external_software:

============================
Installing external software
============================
The Hylleraas Software Platform on its own does not provide any functionality
for performing chemical calculations. Instead, it is designed to be used *with*
other software packages to facilitate setup and execution of simulations.

In order to use the Hylleraas Software Platform, you will need to have some
external software installed. The following sections contain installation
instructions for all of the codes which the HSP is interfaced to.

External Software
#################
The following external software packages are supported by the Hylleraas
Software Platform:


============================ ===================================================
Quantum Chemistry Codes
============================ ===================================================
:ref:`installing_cp2k`
:ref:`installing_crest`
:ref:`installing_dalton`
:ref:`installing_lsdalton`
:ref:`installing_hyqd`
:ref:`installing_london`
:ref:`installing_mrchem`
:ref:`installing_respect`
:ref:`installing_vasp`
============================ ===================================================

============================ ===================================================
Molecular Dynamics Codes
============================ ===================================================
:ref:`installing_lammps`
============================ ===================================================

============================ ===================================================
Machine Learning Codes
============================ ===================================================
:ref:`installing_deepmd`
:ref:`installing_nequip`
============================ ===================================================
