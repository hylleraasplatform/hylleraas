(installing_hyqd)=
# Installing HyQD for the Hylleraas Software Platform
Please see the [HyQD installation instructions](https://hyqd.github.io/quantum-systems/get_started.html) for information on how to compile HyQD.
