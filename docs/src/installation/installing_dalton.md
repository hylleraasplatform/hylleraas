(installing_dalton)=
# Installing Dalton for the Hylleraas Software Platform
Please see the [Dalton installation instructions](https://dalton-installation.readthedocs.io/en/latest/) for information on how to compile Dalton.
