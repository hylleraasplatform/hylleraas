(installing_vasp)=
# Installing VASP for the Hylleraas Software Platform
Please see the [VASP installation instructions](https://www.vasp.at/wiki/index.php/Installing_VASP.6.X.X) for information on how to compile VASP.
