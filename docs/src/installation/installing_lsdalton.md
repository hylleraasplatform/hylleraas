(installing_lsdalton)=
# Installing LSDalton for the Hylleraas Software Platform
Please see the [LSDalton installation instructions](https://gitlab.com/dalton/lsdalton/tree/release/2020) for information on how to compile LSDalton.
