(installing_crest)=
# Installing CREST for the Hylleraas Software Platform
Please see the [CREST installation instructions](https://crest-lab.github.io/crest-docs/page/installation) for information on how to compile CREST.
