(installing_cp2k)=
# Installing CP2K for the Hylleraas Software Platform
Please see the [CP2K installation instructions](https://www.cp2k.org/howto) for information on
how to compile CP2K.
