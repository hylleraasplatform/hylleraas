(installing_nequip)=
# Installing NequIP for the Hylleraas Software Platform
Please see the [NequIP installation instructions](https://nequip.readthedocs.io/en/latest/installation/install.html) for information on how to compile NequIP.
