(installing_deepmd)=
# Installing DeePMD-kit for the Hylleraas Software Platform
Please see the [DeePMD-kit installation instructions](https://docs.deepmodeling.com/projects/deepmd/en/master/install/index.html) for information on how to compile DeePMD-kit.
