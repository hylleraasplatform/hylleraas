(installing_mrchem)=
# Installing MRChem for the Hylleraas Software Platform
Please see the [MRChem installation instructions](https://mrchem.readthedocs.io/en/latest/installation.html) for information on how to compile MRChem.
