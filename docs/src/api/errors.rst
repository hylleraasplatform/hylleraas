Errors
===================

.. currentmodule:: hylleraas.errors

.. autoclass:: GenericError
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: InputError
   :members:
   :undoc-members:
   :show-inheritance:

.. autoclass:: NotFoundError
   :members:
   :undoc-members:
   :show-inheritance:
