API Reference
=============

.. toctree::
   :maxdepth: 2

   compute
   processing
   errors
   method
   utils
