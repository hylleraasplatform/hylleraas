{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "90a7aa8c",
   "metadata": {},
   "source": [
    "# Quick start guide"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d944ec83",
   "metadata": {},
   "source": [
    "This quick start guide will give a (highly incomplete) overview of the *Hylleraas Software Platform* (HSP) and how to use it to perform simple calculations. As an example we will look at the single-point energy&ndash;gradient calculation of a caffeine molecule using the semi-empirical tight binding xTB method.\n",
    "\n",
    "<a id='install_note'></a>\n",
    "\n",
    "```{eval-rst}\n",
    ".. admonition:: Note\n",
    "\n",
    "   This guide requires the *Hylleraas Software Platform* to be installed. Please refer to :ref:`installation` for information about how to install the HSP.\n",
    "\n",
    "   This guide also requires the xTB software to be installed. Please refer to :ref:`installing_xtb` for information about how to install xTB.\n",
    "\n",
    "   Normally, **all** necessary software for this tutorial may be installed by running the following commands in the terminal:\n",
    "\n",
    "    .. code-block:: bash\n",
    "\n",
    "        python3 -m pip install git+https://gitlab.com/hylleraasplatform/hylleraas.git\n",
    "\n",
    "        conda create -n conda_xtb\n",
    "        conda install -c conda-forge -n conda_xtb xtb\n",
    "```\n",
    "\n",
    "## General usage workflow\n",
    "Usage of the HSP typically follows the following workflow:\n",
    "\n",
    "1. **Define the molecular system**:  \n",
    "&#9; Build the molecular geometry of the system you want to study. \n",
    "\n",
    "2. **Establish the simulation method**:   \n",
    "&#9; Choose one of the available software to perform the calculation with.\n",
    "\n",
    "    - 2a. (*Optional*) **Setup simulation options**:  \n",
    "    Choose *how* to run the simulation and which (and how many) resources to use.\n",
    "\n",
    "3. **Run the simulation**:  \n",
    "&#9; Submit the calculation with the HSP to a computing resource (the local machine, or a remote cluster).\n",
    "\n",
    "4. **Analyze the results**:  \n",
    "&#9; Retrieve the results analyze them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "70c541ad",
   "metadata": {},
   "outputs": [],
   "source": [
    "import hylleraas as hsp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a2ff49c",
   "metadata": {},
   "source": [
    "### 1. Defining the system&mdash;`Molecule` objects\n",
    "The first step of any chemical calculation is to define the molecular system you want to study. This is typically done by providing the molecular geometry of the system. In the HSP, this is done by providing a molecular topology and creating a `Molecule` object. A `Molecule` object is a representation of the molecular system that can be used to perform calculations. Note that the HSP `Molecule` object is not necessarily bound together in the normal chemical covalent sense, but rather a collection of atoms and their positions in space that may represent one or more *actual* chemical molecules.\n",
    "\n",
    "The topology of the molecule can be provided in a variety of ways, but here we will generate it from a [SMILES string](https://en.wikipedia.org/wiki/Simplified_molecular-input_line-entry_system). For an in-depth overview of the different ways to generate `Molecule` objects, see the [molecule user guide](molecule.html). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "id": "9108b9e9",
   "metadata": {},
   "outputs": [],
   "source": [
    "caffeine = hsp.Molecule(\"CN1C=NC2=C1C(=O)N(C(=O)N2C)C\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af01d821",
   "metadata": {},
   "source": [
    "The molecule can be inspected after creation to verify that it was created correctly. The list of atoms and coordinates are available as `atoms` and `coordinates` attributes. and viewed in a 3D viewer to ensure that the geometry is correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 78,
   "id": "3e12889b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['C', 'N', 'C', 'N', 'C', 'C', 'C', 'O', 'N', 'C', 'O', 'N', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H']\n",
      "[[ 3.248941e+00 -1.983300e-02 -1.814310e-01]\n",
      " [ 1.991275e+00 -7.290750e-01 -1.200000e-05]\n",
      " [ 1.901331e+00 -2.052956e+00  2.161750e-01]\n",
      " [ 5.878090e-01 -2.312110e+00  3.288030e-01]\n",
      " [-1.249870e-01 -1.179226e+00  1.866490e-01]\n",
      " [ 7.816680e-01 -1.545080e-01 -2.609800e-02]\n",
      " [ 3.814430e-01  1.153945e+00 -2.132200e-01]\n",
      " [ 1.234012e+00  2.059833e+00 -4.040730e-01]\n",
      " [-9.361010e-01  1.419828e+00 -1.841670e-01]\n",
      " [-1.843850e+00  4.255600e-01  2.378200e-02]\n",
      " [-3.058476e+00  7.536670e-01  3.731900e-02]\n",
      " [-1.426824e+00 -8.451250e-01  2.039630e-01]\n",
      " [-2.414133e+00 -1.887063e+00  4.238160e-01]\n",
      " [-1.356099e+00  2.781776e+00 -3.787700e-01]\n",
      " [ 3.192151e+00  9.862720e-01  2.864110e-01]\n",
      " [ 4.058980e+00 -5.556260e-01  3.959420e-01]\n",
      " [ 3.554363e+00  3.750000e-04 -1.238756e+00]\n",
      " [ 2.730861e+00 -2.773587e+00  2.861950e-01]\n",
      " [-3.309331e+00 -1.744555e+00 -2.176430e-01]\n",
      " [-2.796307e+00 -1.873058e+00  1.486249e+00]\n",
      " [-1.988135e+00 -2.869331e+00  1.805370e-01]\n",
      " [-8.080920e-01  3.261013e+00 -1.215732e+00]\n",
      " [-1.132872e+00  3.352243e+00  5.453260e-01]\n",
      " [-2.467629e+00  2.801540e+00 -5.412640e-01]]\n",
      "Units({'style': 'angstrom', 'length': Quantity(unit='angstrom', value=0.529177210903, symbol='angstrom')})\n"
     ]
    }
   ],
   "source": [
    "print(caffeine.atoms)\n",
    "print(caffeine.coordinates)\n",
    "print(caffeine.units)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b8438c87",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/3dmoljs_load.v0": "<div id=\"3dmolviewer_1709037080315856\"  style=\"position: relative; width: 640px; height: 400px;\">\n        <p id=\"3dmolwarning_1709037080315856\" style=\"background-color:#ffcccc;color:black\">You appear to be running in JupyterLab (or JavaScript failed to load for some other reason).  You need to install the 3dmol extension: <br>\n        <tt>jupyter labextension install jupyterlab_3dmol</tt></p>\n        </div>\n<script>\n\nvar loadScriptAsync = function(uri){\n  return new Promise((resolve, reject) => {\n    //this is to ignore the existence of requirejs amd\n    var savedexports, savedmodule;\n    if (typeof exports !== 'undefined') savedexports = exports;\n    else exports = {}\n    if (typeof module !== 'undefined') savedmodule = module;\n    else module = {}\n\n    var tag = document.createElement('script');\n    tag.src = uri;\n    tag.async = true;\n    tag.onload = () => {\n        exports = savedexports;\n        module = savedmodule;\n        resolve();\n    };\n  var firstScriptTag = document.getElementsByTagName('script')[0];\n  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\n});\n};\n\nif(typeof $3Dmolpromise === 'undefined') {\n$3Dmolpromise = null;\n  $3Dmolpromise = loadScriptAsync('https://cdnjs.cloudflare.com/ajax/libs/3Dmol/2.0.4/3Dmol-min.js');\n}\n\nvar viewer_1709037080315856 = null;\nvar warn = document.getElementById(\"3dmolwarning_1709037080315856\");\nif(warn) {\n    warn.parentNode.removeChild(warn);\n}\n$3Dmolpromise.then(function() {\nviewer_1709037080315856 = $3Dmol.createViewer(document.getElementById(\"3dmolviewer_1709037080315856\"),{backgroundColor:\"white\"});\nviewer_1709037080315856.zoomTo();\n\tviewer_1709037080315856.clear();\n\tviewer_1709037080315856.addModel(\"24\\n\\nC 3.288063000000 -0.060723000000 0.203562000000\\nN 2.053908000000 0.695622000000 0.016991000000\\nC 1.880095000000 2.021190000000 -0.090368000000\\nN 0.576551000000 2.313047000000 -0.253722000000\\nC -0.133170000000 1.145380000000 -0.254427000000\\nC 0.819772000000 0.162421000000 -0.083823000000\\nC 0.343395000000 -1.139192000000 -0.049863000000\\nO 1.188260000000 -2.055308000000 0.104360000000\\nN -0.971119000000 -1.378284000000 -0.179116000000\\nC -1.869441000000 -0.403033000000 -0.343174000000\\nO -3.114878000000 -0.659380000000 -0.463642000000\\nN -1.443179000000 0.870192000000 -0.380782000000\\nC -2.383488000000 1.965070000000 -0.557433000000\\nC -1.412949000000 -2.757684000000 -0.136165000000\\nH 3.397565000000 -0.419386000000 1.245418000000\\nH 4.148646000000 0.583627000000 -0.010108000000\\nH 3.264613000000 -0.977914000000 -0.436326000000\\nH 2.702099000000 2.736153000000 -0.047164000000\\nH -3.403707000000 1.562920000000 -0.555096000000\\nH -2.196759000000 2.780696000000 0.144802000000\\nH -2.201621000000 2.343722000000 -1.607167000000\\nH -1.259461000000 -3.101389000000 0.912554000000\\nH -2.483437000000 -2.842797000000 -0.438719000000\\nH -0.789757000000 -3.384948000000 -0.809456000000\\n\",\"xyz\");\n\t\tviewer_1709037080315856.getModel().setStyle({\"stick\": {\"radius\": 0.1}, \"sphere\": {\"scale\": 0.25}});\n\tviewer_1709037080315856.setBackgroundColor(\"#17d9ff\");\n\tviewer_1709037080315856.zoomTo();\nviewer_1709037080315856.render();\n});\n</script>",
      "text/html": [
       "<div id=\"3dmolviewer_1709037080315856\"  style=\"position: relative; width: 640px; height: 400px;\">\n",
       "        <p id=\"3dmolwarning_1709037080315856\" style=\"background-color:#ffcccc;color:black\">You appear to be running in JupyterLab (or JavaScript failed to load for some other reason).  You need to install the 3dmol extension: <br>\n",
       "        <tt>jupyter labextension install jupyterlab_3dmol</tt></p>\n",
       "        </div>\n",
       "<script>\n",
       "\n",
       "var loadScriptAsync = function(uri){\n",
       "  return new Promise((resolve, reject) => {\n",
       "    //this is to ignore the existence of requirejs amd\n",
       "    var savedexports, savedmodule;\n",
       "    if (typeof exports !== 'undefined') savedexports = exports;\n",
       "    else exports = {}\n",
       "    if (typeof module !== 'undefined') savedmodule = module;\n",
       "    else module = {}\n",
       "\n",
       "    var tag = document.createElement('script');\n",
       "    tag.src = uri;\n",
       "    tag.async = true;\n",
       "    tag.onload = () => {\n",
       "        exports = savedexports;\n",
       "        module = savedmodule;\n",
       "        resolve();\n",
       "    };\n",
       "  var firstScriptTag = document.getElementsByTagName('script')[0];\n",
       "  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);\n",
       "});\n",
       "};\n",
       "\n",
       "if(typeof $3Dmolpromise === 'undefined') {\n",
       "$3Dmolpromise = null;\n",
       "  $3Dmolpromise = loadScriptAsync('https://cdnjs.cloudflare.com/ajax/libs/3Dmol/2.0.4/3Dmol-min.js');\n",
       "}\n",
       "\n",
       "var viewer_1709037080315856 = null;\n",
       "var warn = document.getElementById(\"3dmolwarning_1709037080315856\");\n",
       "if(warn) {\n",
       "    warn.parentNode.removeChild(warn);\n",
       "}\n",
       "$3Dmolpromise.then(function() {\n",
       "viewer_1709037080315856 = $3Dmol.createViewer(document.getElementById(\"3dmolviewer_1709037080315856\"),{backgroundColor:\"white\"});\n",
       "viewer_1709037080315856.zoomTo();\n",
       "\tviewer_1709037080315856.clear();\n",
       "\tviewer_1709037080315856.addModel(\"24\\n\\nC 3.288063000000 -0.060723000000 0.203562000000\\nN 2.053908000000 0.695622000000 0.016991000000\\nC 1.880095000000 2.021190000000 -0.090368000000\\nN 0.576551000000 2.313047000000 -0.253722000000\\nC -0.133170000000 1.145380000000 -0.254427000000\\nC 0.819772000000 0.162421000000 -0.083823000000\\nC 0.343395000000 -1.139192000000 -0.049863000000\\nO 1.188260000000 -2.055308000000 0.104360000000\\nN -0.971119000000 -1.378284000000 -0.179116000000\\nC -1.869441000000 -0.403033000000 -0.343174000000\\nO -3.114878000000 -0.659380000000 -0.463642000000\\nN -1.443179000000 0.870192000000 -0.380782000000\\nC -2.383488000000 1.965070000000 -0.557433000000\\nC -1.412949000000 -2.757684000000 -0.136165000000\\nH 3.397565000000 -0.419386000000 1.245418000000\\nH 4.148646000000 0.583627000000 -0.010108000000\\nH 3.264613000000 -0.977914000000 -0.436326000000\\nH 2.702099000000 2.736153000000 -0.047164000000\\nH -3.403707000000 1.562920000000 -0.555096000000\\nH -2.196759000000 2.780696000000 0.144802000000\\nH -2.201621000000 2.343722000000 -1.607167000000\\nH -1.259461000000 -3.101389000000 0.912554000000\\nH -2.483437000000 -2.842797000000 -0.438719000000\\nH -0.789757000000 -3.384948000000 -0.809456000000\\n\",\"xyz\");\n",
       "\t\tviewer_1709037080315856.getModel().setStyle({\"stick\": {\"radius\": 0.1}, \"sphere\": {\"scale\": 0.25}});\n",
       "\tviewer_1709037080315856.setBackgroundColor(\"#17d9ff\");\n",
       "\tviewer_1709037080315856.zoomTo();\n",
       "viewer_1709037080315856.render();\n",
       "});\n",
       "</script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "application/3dmoljs_load.v0": "<script>\n            $3Dmolpromise.then(function() { //wrap in promise for non-interactive functionality\n                \n                viewer_1709037080315856.render();\n            });\n            </script>",
      "text/html": [
       "<script>\n",
       "            $3Dmolpromise.then(function() { //wrap in promise for non-interactive functionality\n",
       "                \n",
       "                viewer_1709037080315856.render();\n",
       "            });\n",
       "            </script>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "hsp.view_molecule(caffeine);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73890cd5",
   "metadata": {},
   "source": [
    "### 2. Establishing the simulation method&mdash;`Method` objects\n",
    "The next step is to establish the simulation method that will be used to perform the calculation. The HSP provides a variety of *interfaces* to different software packages, and the method object is used to define the software and simulation method that will be used to perform the calculation.\n",
    "\n",
    "Method objects are created by providing the name of the software package and the method that will be used. In this case, we will use the semi-empirical tight binding xTB method, which is available through the `Xtb` interface."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 85,
   "id": "383af5ac",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "WARNING uxtbpy: Failed to retrieve number of atoms. Check input file.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "xtb_method = hsp.Xtb(\n",
    "    {\n",
    "        \"properties\": [\"energy\", \"gradient\"],  # calculate energy and gradients\n",
    "    }\n",
    ")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbbd4f74",
   "metadata": {},
   "source": [
    "The `hsp.Xtb` method interface is called with a dictionary containing options for the simulation. Here we only provide the `properties` key with `\"energy\"` and `\"gradient\"`, meaning we will calculate energies and gradients with default xTB settings."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c09eb9d6",
   "metadata": {},
   "source": [
    "### 2a. Setup simulation options&mdash;`compute_settings` objects\n",
    "Better control over *where* and *how* the simulation is run can be achieved by providing a `compute_settings` object. This object is used to define the resources that will be used to perform the calculation, and how the technicalities of how the calculation will be performed.\n",
    "\n",
    "The compute settings object is created by providing a dictionary with the settings that will be used.\n",
    "\n",
    "```{eval-rst}\n",
    ".. admonition:: Note\n",
    "\n",
    "   If you used the suggested install directives in :ref:`install_note`, you will need to link the conda environment :code:`conda_xtb` to the HSP. This is done by creating a :code:`\"conda\"` compute settings object and providing the name of the conda environment, as is done in the following.\n",
    "```\n",
    " \n",
    "The `compute_settings` settings object can link to executables in different conda environments, to executables in the normal system path, or to executables on a remote server such as a SLURM supercomputer cluster. For more information about the different options, see the [compute settings user guide](compute_settings.html).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 103,
   "id": "ede18198",
   "metadata": {},
   "outputs": [],
   "source": [
    "settings_conda_xtb = hsp.create_compute_settings(\n",
    "    \"conda\",\n",
    "    conda_env=\"conda_xtb\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "844c5bdc",
   "metadata": {},
   "source": [
    "Using the specific `compute_settings` object with a method object is achieved by providing the `compute_settings` object when creating the method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 104,
   "id": "8046c71d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "DEBUG: creating directory /Users/mortenledum/Documents/hsp_devdoc/hylleraas/docs/src/user_guide\n",
      "DEBUG: removing file /Users/mortenledum/Documents/hsp_devdoc/hylleraas/docs/src/user_guide/xtbrestart\n",
      "DEBUG: removing file /Users/mortenledum/Documents/hsp_devdoc/hylleraas/docs/src/user_guide/hessian\n",
      "DEBUG: removing file /Users/mortenledum/Documents/hsp_devdoc/hylleraas/docs/src/user_guide/g98.out\n",
      "DEBUG: RUNNING conda run -n conda_xtb xtb --version\n",
      "DEBUG: IN FOLDER /Users/mortenledum/Documents/hsp_devdoc/hylleraas/docs/src/user_guide\n",
      "DEBUG: RESULTS: LocalResult(files_to_parse=[], output_file=None, add_to_results=None, stdout=\"      -----------------------------------------------------------      \\n     |                   =====================                   |     \\n     |                           x T B                           |     \\n     |                   =====================                   |     \\n     |                         S. Grimme                         |     \\n     |          Mulliken Center for Theoretical Chemistry        |     \\n     |                    University of Bonn                     |     \\n      -----------------------------------------------------------      \\n\\n   * xtb version 6.6.1 (8d0f1dd) compiled by 'runner@Mac-1690902400584.local' on 2023-08-01\\n\\n\\n\", stderr='normal termination of xtb\\n\\n', returncode=0, error=None)\n",
      "WARNING uxtbpy: Failed to retrieve number of atoms. Check input file.\n",
      "\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/Users/mortenledum/Documents/hsp_devdoc/hyset/build/__editable__.hyset-0.1-py3-none-any/hyset/local/runner.py:255: UserWarning: normal termination of xtb\n",
      "\n",
      "\n",
      "  warnings.warn(result.stderr)\n"
     ]
    }
   ],
   "source": [
    "xtb_conda_method = hsp.Xtb(\n",
    "    {\n",
    "        \"properties\": [\"energy\", \"gradient\"],  # calculate energy and gradients\n",
    "    },\n",
    "    compute_settings=settings_conda_xtb,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30c239c3",
   "metadata": {},
   "source": [
    "### 3. Running the simulation\n",
    "The simulation is run by providing the `Molecule` object and calling the `.run()` method of the `Method` object. The `.run()` method will submit the calculation to the computing resource defined in the `compute_settings` object, and return a `Result` object.\n",
    "\n",
    "All HSP calculations are evaluated [lazily](https://en.wikipedia.org/wiki/Lazy_evaluation), meaning only the necessary simulation will be ran and once ran, it will not be re-ran if subsequent calls to `.run()` are made (unless explicitly requested via the `force_recompute` flag)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 102,
   "id": "c6e87582",
   "metadata": {},
   "outputs": [],
   "source": [
    "caffeine_xtb_result = xtb_conda_method.run(caffeine, debug=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ff8fc5e",
   "metadata": {},
   "source": [
    "Instead of (or in addition to) the `.run(molecule)` method, you may query directly a desired quantity such as the energy. This is done by the `.get_energy(molecule)` method. Note carefully that the simulation will now **not** be re-done, as this result is already cached."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 93,
   "id": "6b27c226",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-42.121970452276\n"
     ]
    }
   ],
   "source": [
    "energy = xtb_conda_method.get_energy(caffeine)\n",
    "\n",
    "print(energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9fcbcb17",
   "metadata": {},
   "source": [
    "### 4. Analyzing the results\n",
    "The results of the simulation are stored in a `Result` object. The `Result` object contains any and all available properties resulting from the simulation ran. These properties may be accessed and inspected by querying the `Result` object like a normal dictionary.\n",
    "\n",
    "An easy way to list of all available properties in the result object is to call the `.keys()` method of the `Result` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 97,
   "id": "a2e38a5c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "version\n",
      "homo_energy\n",
      "lumo_energy\n",
      "polarisability\n",
      "wiberg_index_matrix\n",
      "dipole_moment\n",
      "energy\n",
      "homo_lumo_gap\n",
      "gradient\n"
     ]
    }
   ],
   "source": [
    "for key in caffeine_xtb_result.keys():\n",
    "    print(key)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "221bd35a",
   "metadata": {},
   "source": [
    "-------------------------"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
