import hyobj
import numpy as np
import pytest
from conftest import check_notebook_errors, execute_notebook, user_guide_dir


@pytest.fixture(scope='session')
def molecule_notebook():
    """Execute the molecule.ipynb notebook and return the output."""
    return execute_notebook(user_guide_dir / 'molecule.ipynb')


def test_molecule_errors(molecule_notebook):
    """Ensure no exceptions are raised when executing the notebook."""
    check_notebook_errors(molecule_notebook)


def test_molecule_d5f33400(molecule_notebook):
    """Test the notebook cell d5f33400."""
    _, _, shell = molecule_notebook

    assert 'hsp' in shell.user_ns


def test_molecule_af56b586(molecule_notebook):
    """Test the notebook cell af56b586."""
    _, _, shell = molecule_notebook

    assert 'formaldehyde' in shell.user_ns
    assert isinstance(shell.user_ns['formaldehyde'], hyobj.Molecule)
    assert len(shell.user_ns['formaldehyde']) == 4
    assert isinstance(shell.user_ns['formaldehyde']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['formaldehyde']._coordinates,
        np.array([
            [0.9612, -0.0531, -0.0380],
            [2.1582, -0.0531, -0.0380],
            [0.4452, -0.8473, 0.3719],
            [0.4452, 0.7411, -0.4478],
        ]))
    assert list(shell.user_ns['formaldehyde']._atoms).count('C') == 1
    assert list(shell.user_ns['formaldehyde']._atoms).count('O') == 1
    assert list(shell.user_ns['formaldehyde']._atoms).count('H') == 2


def test_molecule_7c4f566e(molecule_notebook):
    """Test the notebook cell 7c4f566e."""
    _, _, shell = molecule_notebook

    assert 'water' in shell.user_ns
    assert isinstance(shell.user_ns['water'], hyobj.Molecule)
    assert len(shell.user_ns['water']) == 3
    assert isinstance(shell.user_ns['water']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['water']._coordinates,
        np.array([
            [0.0021, -0.0041, 0.0020],
            [-0.0110, 0.9628, 0.0073],
            [0.8669, 1.3681, 0.0011],
        ]))
    assert list(shell.user_ns['water']._atoms).count('O') == 1
    assert list(shell.user_ns['water']._atoms).count('H') == 2


def test_molecule_734c6b23(molecule_notebook):
    """Test the notebook cell 734c6b23."""
    _, _, shell = molecule_notebook

    assert 'methane' in shell.user_ns
    assert isinstance(shell.user_ns['methane'], hyobj.Molecule)
    assert len(shell.user_ns['methane']) == 5
    assert isinstance(shell.user_ns['methane']._coordinates, np.ndarray)

    assert shell.user_ns['methane'].distance(0, 1) == pytest.approx(1.089, abs=1e-5)
    assert shell.user_ns['methane'].distance(0, 2) == pytest.approx(1.089, abs=1e-5)
    assert shell.user_ns['methane'].distance(0, 3) == pytest.approx(1.089, abs=1e-5)
    assert shell.user_ns['methane'].distance(0, 4) == pytest.approx(1.089, abs=1e-5)

    assert shell.user_ns['methane'].angle(1, 0, 2) == pytest.approx(109.471, abs=1e-2)
    assert shell.user_ns['methane'].angle(1, 0, 3) == pytest.approx(109.471, abs=1e-2)
    assert shell.user_ns['methane'].angle(1, 0, 4) == pytest.approx(109.471, abs=1e-2)

    assert shell.user_ns['methane'].dihedral(3, 2, 0, 1) == pytest.approx(120.0, abs=1e-2)
    assert shell.user_ns['methane'].dihedral(4, 2, 0, 1) == pytest.approx(-120.0, abs=1e-2)

    assert list(shell.user_ns['methane']._atoms).count('C') == 1
    assert list(shell.user_ns['methane']._atoms).count('H') == 4


def test_molecule_e59ba307(molecule_notebook):
    """Test the notebook cell e59ba307."""
    _, _, shell = molecule_notebook

    assert 'ethylene' in shell.user_ns
    assert isinstance(shell.user_ns['ethylene'], hyobj.Molecule)
    assert len(shell.user_ns['ethylene']) == 6
    assert isinstance(shell.user_ns['ethylene']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['ethylene']._coordinates,
        np.array([
            [1.066, -0.055, -0.065],
            [2.398, -0.055, -0.065],
            [0.550, -0.185, -0.949],
            [0.550, 0.076, 0.819],
            [2.914, -0.185, -0.949],
            [2.914, 0.076, 0.819],
        ]))
    assert list(shell.user_ns['ethylene']._atoms).count('C') == 2
    assert list(shell.user_ns['ethylene']._atoms).count('H') == 4


def test_molecule_cf2f3a7d(molecule_notebook):
    """Test the notebook cell cf2f3a7d."""
    _, _, shell = molecule_notebook

    assert 'acetylene' in shell.user_ns
    assert isinstance(shell.user_ns['acetylene'], hyobj.Molecule)
    assert len(shell.user_ns['acetylene']) == 4
    assert isinstance(shell.user_ns['acetylene']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['acetylene']._coordinates,
        np.array([
            [0.60106, 0.000000, 0.000000],
            [-0.60106, 0.000000, 0.000000],
            [1.66106, 0.000000, 0.000000],
            [-1.66106, 0.000000, 0.000000],
        ]))
    assert list(shell.user_ns['acetylene']._atoms).count('C') == 2
    assert list(shell.user_ns['acetylene']._atoms).count('H') == 2


def test_molecule_dfac2a99(molecule_notebook):
    """Test the notebook cell dfac2a99."""
    _, _, shell = molecule_notebook

    assert 'formic_acid' in shell.user_ns
    assert isinstance(shell.user_ns['formic_acid'], hyobj.Molecule)
    assert len(shell.user_ns['formic_acid']) == 5
    assert isinstance(shell.user_ns['formic_acid']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['formic_acid']._coordinates,
        np.array([
            [-1.1685, 0.1825, 0.0000],
            [1.1146, 0.2103, 0.0000],
            [0.0538, -0.3927, 0.0000],
            [-0.0511, -1.4875, 0.0000],
            [-1.1142, 1.1620, 0.0000],
        ]))
    assert shell.user_ns['formic_acid']._atoms == ['O', 'O', 'C', 'H', 'H']


def test_molecule_afc6dea3(molecule_notebook):
    """Test the notebook cell afc6dea3."""
    _, _, shell = molecule_notebook

    assert 'chloromethane' in shell.user_ns
    assert isinstance(shell.user_ns['chloromethane'], hyobj.Molecule)
    assert len(shell.user_ns['chloromethane']) == 5
    assert isinstance(shell.user_ns['chloromethane']._coordinates, np.ndarray)
    assert shell.user_ns['chloromethane'].distance(0, 1) == pytest.approx(1.754, abs=1e-5)
    assert shell.user_ns['chloromethane'].distance(0, 2) == pytest.approx(1.070, abs=1e-5)
    assert shell.user_ns['chloromethane'].distance(0, 3) == pytest.approx(1.070, abs=1e-5)
    assert shell.user_ns['chloromethane'].distance(0, 4) == pytest.approx(1.070, abs=1e-5)

    assert shell.user_ns['chloromethane'].angle(1, 0, 2) == pytest.approx(109.471, abs=1e-2)
    assert shell.user_ns['chloromethane'].angle(1, 0, 3) == pytest.approx(109.471, abs=1e-2)
    assert shell.user_ns['chloromethane'].angle(1, 0, 4) == pytest.approx(109.471, abs=1e-2)

    assert shell.user_ns['chloromethane'].dihedral(3, 2, 0, 1) == pytest.approx(-120.0, abs=1e-2)
    assert shell.user_ns['chloromethane'].dihedral(4, 2, 0, 1) == pytest.approx(120.0, abs=1e-2)

    assert list(shell.user_ns['chloromethane']._atoms).count('C') == 1
    assert list(shell.user_ns['chloromethane']._atoms).count('Cl') == 1
    assert list(shell.user_ns['chloromethane']._atoms).count('H') == 3


def test_molecule_61760efe(molecule_notebook):
    """Test the notebook cell 61760efe."""
    _, _, shell = molecule_notebook

    assert 'benzene' in shell.user_ns
    assert isinstance(shell.user_ns['benzene'], hyobj.Molecule)
    assert len(shell.user_ns['benzene']) == 12
    assert isinstance(shell.user_ns['benzene']._coordinates, np.ndarray)
    np.testing.assert_allclose(
        shell.user_ns['benzene']._coordinates,
        np.array([
            [1.2194, -0.1652, 2.1600],
            [0.6825, -0.0924, 1.2087],
            [-0.7075, -0.0352, 1.1973],
            [-1.2644, -0.0630, 2.1393],
            [-1.3898, 0.0572, -0.0114],
            [-2.4836, 0.1021, -0.0204],
            [-0.6824, 0.0925, -1.2088],
            [-1.2194, 0.1652, -2.1599],
            [0.7075, 0.0352, -1.1973],
            [1.2641, 0.0628, -2.1395],
            [1.3899, -0.0572, 0.0114],
            [2.4836, -0.1022, 0.0205],
        ]))
    assert list(shell.user_ns['benzene']._atoms).count('C') == 6
    assert list(shell.user_ns['benzene']._atoms).count('H') == 6
