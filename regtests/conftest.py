import io
import os
from contextlib import contextmanager, redirect_stderr, redirect_stdout
from pathlib import Path
from typing import Any, Dict, Generator, Tuple, Union

import nbformat
import tqdm
import yaml
from IPython.core.interactiveshell import ExecutionResult, InteractiveShell

user_guide_dir = Path(__file__).parent.parent / 'docs' / 'src' / 'user_guide'
developers_guide_dir = user_guide_dir.parent.parent / 'developers_guide'


@contextmanager
def change_directory(new_directory: Union[str, Path, os.PathLike]) -> Generator[None, None, None]:
    """Change the working directory temporarily.

    Parameters
    ----------
    new_directory : str or Path or os.PathLike
        The directory to change to.

    """
    old_directory: Path = Path.cwd()
    try:
        os.chdir(new_directory)
        yield
    finally:
        os.chdir(old_directory)


@contextmanager
def overwrite_compute_settings(cell_id: str, overwrite: Dict[str, Dict[str, Any]]) -> Generator[None, None, None]:
    """Write an overwrite dictionary to overwrite/compute_settings.yml.

    Deletes the overwrite/ directory after the context manager exits.

    Parameters
    ----------
    cell_id : str
        The cell id to write the overwrite dictionary to.
    overwrite : dict[str, Any]
        A dictionary of variables to overwrite in the notebook compute_settings.
        The dictionary is keyed by the variable name, and the value is the value
        to overwrite the variable with.

    """
    if not overwrite.get(cell_id, None):
        yield
        return

    overwrite_dir: Path = Path('overwrite')
    overwrite_dir.mkdir(exist_ok=True)
    overwrite_path = overwrite_dir / 'compute_settings.yml'

    with open(overwrite_path, 'w', encoding='utf-8') as outfile:
        yaml.dump(overwrite[cell_id], outfile)
    try:
        yield
    finally:
        overwrite_path.unlink()
        overwrite_dir.rmdir()


def execute_notebook(
    notebook_path: Path,
    overwrite: Dict[str, Dict[str, Any]] = {}
) -> Tuple[Dict[str, ExecutionResult], Dict[str, Exception], InteractiveShell]:
    """Load and execute a notebook using nbformat and IPython interactiveshell.

    Parameters
    ----------
    notebook_path : str or Path or os.PathLike
        The path to the notebook to execute.
    overwrite : dict[str, [dict[str, Any]], optional
        A dictionary of dictionaries containing the name and value of variables
        to overwrite in the notebook compute_settings. The outer dictionary is
        keyed by the cell id, and the inner dictionary is keyed by the variable
        name. The value of the inner dictionary is the value to overwrite the
        variable with. This is written to compute_settings.yml in an overwrite/
        directory in the same directory as the notebook.

    Returns
    -------
    output : dict[str, IPython.core.interactiveshell.ExecutionResult]
        A dictionary of the output of each cell, keyed by cell id.
    exceptions : dict[str, Exception]
        A dictionary of any exceptions raised by each cell, keyed by cell id.
    shell : IPython.core.interactiveshell.InteractiveShell
        The IPython shell used to execute the notebook. Contains the notebook
        namespace with all variables defined in the notebook.

    """
    # Ensure we are in the parent directory of the notebook before we execute
    # any cells.
    with change_directory(notebook_path.parent):
        with open(notebook_path, 'r', encoding='utf-8') as infile:
            nb: nbformat.NotebookNode = nbformat.read(infile, as_version=4)

            # Attempt to add "id" properties to each cell if they are not
            # present.
            if not (nb['nbformat'] >= 4 and nb['nbformat_minor'] >= 5):
                nb['nbformat'] = 4
                nb['nbformat_minor'] = 5
                nbformat.validator.normalize(nb)

        shell: InteractiveShell = InteractiveShell.instance()
        output: dict[str, ExecutionResult] = {}
        exceptions: dict[str, Exception] = {}

        for cell in tqdm.tqdm(nb.cells):
            if cell.cell_type == 'code':

                # Places an overwrite/compute_settings.yml file in the current
                # directory to overwrite compute settings in this specific cell
                # if provided in the overwrite dictionary--otherwise does
                # nothing.
                with overwrite_compute_settings(cell.id, overwrite):
                    stdout, stderr = io.StringIO(), io.StringIO()
                    with redirect_stdout(stdout), redirect_stderr(stderr):
                        try:
                            output[cell.id] = shell.run_cell(cell.source, store_history=True)
                        except Exception as e:
                            exceptions[cell.id] = e
                        finally:
                            if cell.id in output:
                                output[cell.id].stdout = stdout.getvalue()
                                output[cell.id].stderr = stderr.getvalue()

    return output, exceptions, shell


def check_notebook_errors(notebook) -> None:
    """Ensure no exceptions are raised after executing the notebook.

    Parameters
    ----------
    notebook : Tuple[Dict[str, ExecutionResult], Dict[str, Exception], InteractiveShell]
        The output of the execute_notebook function.

    Raises
    ------
    AssertionError
        If an exception is raised in any cell.

    """
    output, exceptions, _ = notebook

    for cell_id in output.keys():
        exception = exceptions.get(cell_id, None)

        if exception:
            assert False, f'Exception in cell {cell_id}: {exception}'

        if output[cell_id].stderr:
            assert False, f'Error in cell {cell_id}: {output[cell_id].stderr}'

        if output[cell_id].error_before_exec:
            error_message_before: str = (f'Error before executing cell {cell_id}: '
                                         f'{output[cell_id].error_before_exec}')
            assert False, error_message_before

        if output[cell_id].error_in_exec:
            error_message_during: str = (f'Error executing cell {cell_id}: ' f'{output[cell_id].error_in_exec}')
            assert False, error_message_during
