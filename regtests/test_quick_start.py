import hyif
import hyobj
import hyset
import numpy as np
import pytest
import semantic_version
from conftest import check_notebook_errors, execute_notebook, user_guide_dir


@pytest.fixture(scope='session')
def quick_start_notebook():
    """Execute the quick_start.ipynb notebook and return the output."""
    overwrite = {'ede18198': {'target': 'local', }, '8046c71d': {'target': 'local', }}
    return execute_notebook(user_guide_dir / 'quick_start.ipynb', overwrite)


def test_quick_start_errors(quick_start_notebook):
    """Ensure no exceptions are raised when executing the notebook."""
    check_notebook_errors(quick_start_notebook)


def test_quick_start_70c541ad(quick_start_notebook):
    """Test the notebook cell 70c541ad."""
    _, _, shell = quick_start_notebook

    assert 'hsp' in shell.user_ns


def test_quick_start_9108b9e9(quick_start_notebook):
    """Test the notebook cell 9108b9e9."""
    _, _, shell = quick_start_notebook

    assert 'caffeine' in shell.user_ns
    assert isinstance(shell.user_ns['caffeine'], hyobj.Molecule)
    assert len(shell.user_ns['caffeine']) == 24
    assert isinstance(shell.user_ns['caffeine']._coordinates, np.ndarray)


def test_quick_start_3e12889b(quick_start_notebook):
    """Test the notebook cell 3e12889b."""
    output, _, _ = quick_start_notebook
    cell_id = '3e12889b'
    assert output[cell_id].stdout.count("'C'") == 8
    assert output[cell_id].stdout.count("'H'") == 10
    assert output[cell_id].stdout.count("'N'") == 4
    assert output[cell_id].stdout.count("'O'") == 2
    assert output[cell_id].stdout.count('[') == 26


def test_quick_start_383af5ac(quick_start_notebook):
    """Test the notebook cell 383af5ac."""
    _, _, shell = quick_start_notebook

    assert 'xtb_method' in shell.user_ns
    assert isinstance(shell.user_ns['xtb_method'], hyif.quantumchemistry.xtb.Xtb)
    assert isinstance(shell.user_ns['xtb_method'].compute_settings, hyset.LocalArch)


def test_quick_start_ede18198(quick_start_notebook):
    """Test the notebook cell ede18198."""
    _, _, shell = quick_start_notebook

    assert 'settings_conda_xtb' in shell.user_ns

    # This compute settings object is overwritten in the notebook, so it is not
    # a ComputeSettingsConda, but rather a LocalArch.
    assert isinstance(shell.user_ns['settings_conda_xtb'], hyset.local.LocalArch)


def test_quick_start_8046c71d(quick_start_notebook):
    """Test the notebook cell 8046c71d."""
    _, _, shell = quick_start_notebook

    assert 'xtb_conda_method' in shell.user_ns
    assert isinstance(shell.user_ns['xtb_conda_method'], hyif.quantumchemistry.xtb.Xtb)


def test_quick_start_c6e87582(quick_start_notebook):
    """Test the notebook cell c6e87582."""
    _, _, shell = quick_start_notebook

    assert 'caffeine_xtb_result' in shell.user_ns
    assert isinstance(shell.user_ns['caffeine_xtb_result'], dict)

    result = shell.user_ns['caffeine_xtb_result']
    for key in ('energy', 'homo_lumo_gap', 'gradient', 'version'):
        assert key in result

    assert result['homo_lumo_gap'] == pytest.approx(3.4, abs=0.1)
    assert result['gradient'].size == 72
    assert semantic_version.Version(result['version']).major >= 6


def test_quick_start_6b27c226(quick_start_notebook):
    """Test the notebook cell 6b27c226."""
    output, _, shell = quick_start_notebook

    cell_id = '6b27c226'
    assert output[cell_id].stdout
    assert float(output[cell_id].stdout.rstrip())

    energy = float(output[cell_id].stdout.rstrip())
    assert energy == pytest.approx(-42.1, abs=0.1)
    assert energy == pytest.approx(shell.user_ns['energy'])


def test_quick_start_a2e38a5c(quick_start_notebook):
    """Test the notebook cell a2e38a5c."""
    output, _, _ = quick_start_notebook

    for key in (
            'version',
            'energy',
            'homo_lumo_gap',
            'gradient',
            'gradient_norm',
            'electrons',
            'atoms',
    ):
        assert key in output['a2e38a5c'].stdout
