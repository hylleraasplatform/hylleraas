from abc import ABC, abstractmethod
from typing import Dict, List, Union

import numpy as np
from qcelemental import PhysicalConstantsContext

from .molecule import Molecule

constants = PhysicalConstantsContext('CODATA2018')

# class Output_blueprint(ABC):
#     """Factory class for hylleraas Output instancens"""
#     @property
#     @abstractmethod
#     def success(self) -> bool:
#         pass

#     @property
#     @abstractmethod
#     def energy(self) -> float:
#         pass

#     @property
#     @abstractmethod
#     def final_geometry(self) -> Union[List[float], np.ndarray]:
#         pass

#     @property
#     @abstractmethod
#     def gradient(self) -> np.ndarray:
#         pass

#     @property
#     @abstractmethod
#     def frequencies(self) -> np.ndarray:
#         pass

#     @property
#     @abstractmethod
#     def ir_intensities(self) -> np.ndarray:
#         pass

#     @property
#     @abstractmethod
#     def raman_intensities(self) -> np.ndarray:
#         pass


class Output_Dalton:
    """Dalton output class"""

    def __init__(self, result):
        self.result = result

    @property
    def success(self) -> bool:
        return True

    @property
    def energy(self) -> float:
        return self.result.energy

    @property
    def final_geometry(self) -> Union[List[float], np.ndarray]:
        return self.result.final_geometry

    @property
    def coordinates(self) -> Union[List[float], np.ndarray]:
        return self.result.final_geometry

    @property
    def gradient(self) -> np.ndarray:
        return self.result.gradients

    @property
    def frequencies(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def ir_intensities(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def raman_intensities(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def filename(self) -> str:
        return self.result.filename

    @property
    def summary(self) -> Dict:
        """Extract information about geometry optimization from Dalton outputfile."""
        summary_dict = {}
        summary_dict['thresh_energy'] = None  # could not find this information in output-file
        summary_dict['steps'] = -1
        energies = []
        gradnorms = []
        with open(f'{self.result.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'BFGS update' in line:
                    summary_dict['method'] = 'BFGS'
                if 'Convergence threshold for gradient set to' in line:
                    summary_dict['rms_grad'] = float(line.split()[7].replace('D', 'E'))
                    summary_dict['max_grad'] = summary_dict['rms_grad']
                if 'Convergence threshold for energy set to' in line:
                    summary_dict['thresh_energy'] = float(line.split()[7].replace('D', 'E'))
                if 'Convergence threshold for step set to' in line:
                    summary_dict['rms_step'] = float(line.split()[7].replace('D', 'E'))
                    summary_dict['max_step'] = summary_dict['rms_step']
                if 'Geometry converged in ' in line:
                    summary_dict['steps'] = int(line.split()[4])

                if ('Iter     Energy        Change       GradNorm  Index   StepLen    TrustRad #Rej' in line):
                    output_file.readline()
                    # workaround for reading the optimization table summary
                    check_iter = True
                    check_en = True
                    check_line = 9
                    while check_iter and check_en and check_line == 9:
                        split_line = output_file.readline().split()
                        try:
                            int(split_line[1])
                        except Exception:
                            check_iter = False
                        else:
                            check_iter = True

                        try:
                            float(split_line[2])
                        except Exception:
                            check_en = False
                        else:
                            check_en = True

                        check_line = len(split_line)

                        if check_line == 9:
                            energies.append(float(split_line[2]))
                            gradnorms.append(float(split_line[4]))

        if len(energies) != summary_dict['steps']:
            print(summary_dict['steps'])
            print(energies)
            raise Exception('Dalton geometry output not consistent')

        if len(energies) > 0:
            summary_dict['energies'] = energies
            summary_dict['gradient_norms'] = gradnorms
            summary_dict['final_energy'] = energies[-1]
            summary_dict['final_gradient_norm'] = gradnorms[-1]
            if len(energies) > 1:
                summary_dict['final_energy_change'] = energies[-2] - \
                    energies[-1]

        return summary_dict


class Output_LSDalton:
    """LSDalton output class"""

    def __init__(self, result, new_molecule):
        self.result = result
        self.new_molecule = new_molecule

    @property
    def success(self) -> bool:
        return True

    @property
    def energy(self) -> float:
        return self.result.energy

    @property
    def final_geometry(self) -> Union[List[float], np.ndarray]:
        return self.result.final_geometry

    @property
    def coordinates(self) -> Union[List[float], np.ndarray]:
        return self.result.final_geometry

    @property
    def ir_intensities(self) -> np.ndarray:
        return self.result.ir_intensities

    @property
    def raman_intensities(self) -> np.ndarray:
        return self.result.raman_intensities

    @property
    def frequencies(self) -> np.ndarray:
        return self.result.frequencies

    @property
    def filename(self) -> str:
        return self.result.filename

    @property
    def gradient(self) -> np.ndarray:
        return self.result.gradients

    @property
    def molecule(self) -> Molecule:
        return self.new_molecule

    @property
    def final_molecule(self) -> Molecule:
        return self.new_molecule

    @property
    def summary(self) -> Dict:
        """Extract information about geometry optimization from LSDalton outputfile."""
        summary_dict = {}
        summary_dict['thresh_energy'] = None  # could not find this information in output-file
        summary_dict['steps'] = -1
        energies = []
        gradnorms = []
        with open(f'{self.result.filename}.out', 'r') as output_file:
            for line in output_file:
                if 'BFGS update' in line:
                    summary_dict['method'] = 'BFGS'
                if 'Root-mean-square gradient threshold set to' in line:
                    summary_dict['rms_grad'] = float(line.split()[6].replace('D', 'E'))
                if 'Maximum gradient element threshold set to' in line:
                    summary_dict['max_grad'] = float(line.split()[7].replace('D', 'E'))
                if 'Root-mean-square step threshold set to' in line:
                    summary_dict['rms_step'] = float(line.split()[6].replace('D', 'E'))
                if 'Maximum step element threshold set to' in line:
                    summary_dict['max_step'] = float(line.split()[7].replace('D', 'E'))
                if 'Geometry converged in ' in line:
                    summary_dict['steps'] = int(line.split()[3])

                if ('Iter      Energy        Change       GradNorm  Index   StepLen   TrustRad #Rej' in line):
                    output_file.readline()
                    # workaround for reading the optimization table summary
                    check_iter = True
                    check_en = True
                    check_line = 8
                    while check_iter and check_en and check_line == 8:
                        split_line = output_file.readline().split()
                        try:
                            int(split_line[0])
                        except Exception:
                            check_iter = False
                        else:
                            check_iter = True

                        try:
                            float(split_line[1])
                        except Exception:
                            check_en = False
                        else:
                            check_en = True

                        check_line = len(split_line)

                        if check_line == 8:
                            energies.append(float(split_line[1]))
                            gradnorms.append(float(split_line[3]))

        if len(energies) != summary_dict['steps']:
            raise Exception('LSDalton geometry output not consistent')

        if len(energies) > 0:
            summary_dict['energies'] = energies
            summary_dict['gradient_norms'] = gradnorms
            summary_dict['final_energy'] = energies[-1]
            summary_dict['final_gradient_norm'] = gradnorms[-1]
            if len(energies) > 1:
                summary_dict['final_energy_change'] = energies[-2] - \
                    energies[-1]

        return summary_dict


class Output_Geometric:
    """GeomeTRIC output class"""

    def __init__(self, result, new_molecule, geo_summary):
        self.result = result
        self.new_molecule = new_molecule
        self.geo_summary = geo_summary

    @property
    def energy(self) -> float:
        return self.result['energy']

    @property
    def final_geometry(self) -> Union[List[float], np.ndarray]:
        return self.result['final_geometry']

    @property
    def molecule(self) -> Molecule:
        return self.new_molecule

    @property
    def final_molecule(self) -> Molecule:
        return self.new_molecule

    @property
    def summary(self) -> Dict:
        return self.geo_summary


class Output_London:
    """London output class"""

    def __init__(self, filename, number_of_atoms):
        self.filename = filename
        self.number_of_atoms = number_of_atoms

    @property
    def success(self) -> bool:
        return True

    @property
    def energy(self) -> float:
        """Extract energy from London output file"""
        with open(f'{self.filename}', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if '(final) Total:' in line:
                val = float(line.split()[2])
                return val

    @property
    def final_geometry(self) -> Union[List[float], np.ndarray]:
        pass

    @property
    def gradient(self) -> np.ndarray:
        """Extract energy from London output file"""
        gradient = np.zeros((self.number_of_atoms, 3))
        i = 0
        with open(f'{self.filename}', 'r') as output_file:
            for line in output_file:
                #if "(SCF) Hellmann-Feynman forces:" in line:
                #    for i in range(0, self.number_of_atoms):
                #        split_line = (output_file.readline().replace(
                #            "(", " ").replace(")", " ").replace(",",
                #                                                " ").split())
                #        gradient[i][0] = (float(split_line[-3]) *
                #                          constants.bohr2angstroms)
                #        gradient[i][1] = (float(split_line[-2]) *
                #                          constants.bohr2angstroms)
                #        gradient[i][2] = (float(split_line[-1]) *
                #                          constants.bohr2angstroms)
                if 'Molecular Energy Gradient:' in line:
                    found_grad = True
                if 'Ftotal' in line:
                    split_line = line.replace(',', ' ').replace(')', ' ').replace('(', ' ').split()
                    gradient[i][0] = (float(split_line[-3]) * constants.bohr2angstroms)
                    gradient[i][1] = (float(split_line[-2]) * constants.bohr2angstroms)
                    gradient[i][2] = (float(split_line[-1]) * constants.bohr2angstroms)
                    i = i + 1

        return gradient

    @property
    def total_spin(self) -> float:
        """Extract total spin from London output file"""
        with open(f'{self.filename}', 'r') as output_file:
            lines = output_file.readlines()
        for line in lines:
            if 'Total spin quantum number' in line:
                val = float(line.split()[6])
                return val

    @property
    def frequencies(self) -> np.ndarray:
        pass

    @property
    def ir_intensities(self) -> np.ndarray:
        pass

    @property
    def raman_intensities(self) -> np.ndarray:
        pass
