from typing import List, Optional, Sequence, Tuple, Union

import numpy as np
from qcelemental import PhysicalConstantsContext, periodictable

# from .convert import zmat2xyz
from .utils import zmat2xyz


def read_mol_from_string(string: str) -> Tuple[bool, list]:
    """Read molecule from xyz-string.

    Parameters
    ----------
    string : str
        string containing molecular structure

    Returns
    -------
    string
        filetype
    list
        list of atoms, and coordinates, if input is a xyz string or zmat  if input is a zmat string

    """
    lines = list(string.strip().split('\n'))
    # a single string, i.e. number of atoms

    is_xyz = False
    if lines[0].count(' ') == 0:
        # check if zmat or xyz
        is_xyz = lines[0].isdigit()

        if is_xyz:
            filetype = 'xyz'
            num_atoms = int(lines.pop(0))
            # remove comment line too
            lines.pop(0)
            if len(lines) < num_atoms:
                raise Exception('number of atoms not correct')
            elif len(lines) > num_atoms:
                warnings.warn(f'only {num_atoms} out of {len(lines)} coordinates read.')
    else:
        num_atoms = len(lines)
        is_xyz = True
        filetype = 'xyz'

    if is_xyz:
        atoms = []
        coordinates = np.zeros((num_atoms, 3))
        for i, line in enumerate(lines):
            line = list(filter(None, line.strip().split(' ')))
            atom, *xyz = line
            if len(xyz) != 3:
                raise Exception('only xyz supported')
            atoms.append(atom)
            coordinates[i] = np.array(list(map(float, xyz)), dtype=float)

        return filetype, [atoms, coordinates]
    else:
        try:
            lines2 = [x for x in lines if x != []]
            zmat = []
            for line in lines2:
                if line.split() == []:
                    continue
                zmat.append(line.split())
            filetype = 'zmat'
            return filetype, zmat
        except Exception:
            raise Exception('input string neither in xyz nor in zmat format')


def read_mol_from_file(filename: str, filetype: str):
    """Read molecule from file.

    Parameters
    ----------
    filename : str
        filename for molecular structure
    filetype : str
        filetype

    Returns
    -------
    list
        list of atoms
    list
        list of coordinates

    """
    if filetype == 'cif':
        try:
            from Bio.PDB.MMCIFParser import MMCIFParser
        except Exception:
            ImportError('cif reading options requires installation of biopython ( pip install biopython )')

        parser = MMCIFParser()
        structure = parser.get_structure('mymolecule', filename)
        atoms = []
        coordinates = []
        for atom in structure.get_atoms():
            atoms.append(atom.element.lower().capitalize())
            coordinates.append(atom.coord)

    elif filetype == 'pdb':
        try:
            from Bio.PDB.PDBParser import PDBParser
        except Exception:
            ImportError('pdb reading options requires installation of biopython ( pip install biopython )')

        parser = PDBParser(PERMISSIVE=False)
        structure = parser.get_structure('mymolecule', filename)
        atoms = []
        coordinates = []
        for atom in structure.get_atoms():
            atoms.append(atom.element.lower().capitalize())
            coordinates.append(atom.coord)

    elif filetype == 'zmat':
        zmat = read_zmat(filename)
        # return zmat
        atoms, coordinates = zmat2xyz(zmat)

        # atoms = [ zmat[i][0].lower().title() for i in range(0, len(zmat))      ]

        # if not gen_xyz_from_zmat:
        #     atoms, coordinates = self.zmat2xyz(self.zmat)

    elif filetype == 'xyz':
        atoms, coordinates = read_xyz(filename)

    else:
        raise Exception('only cif, zmat or xyz supported')

    return atoms, coordinates


def read_xyz(filename: str) -> Tuple[List[str], Union[List[float], np.ndarray]]:
    """Read molecular structure from xyz file.

    Parameters
    ----------
    filename : str
        filename for molecular structure

    Returns
    -------
    tuple
        (atoms, coordinates)

    """
    #if not os.path.isabs(filename):
    #    os.path.join(self.settings.work_dir, filename)
    with open(filename, 'r') as myfile:
        lines = myfile.read().split('\n')
    try:
        num_atoms = int(lines[0])
    except Exception:
        print(f'file {filename} not a proper xyz-file')
    coordinates = []
    atoms = []
    for line in lines[2:num_atoms + 2]:
        split_line = line.split()
        try:
            coordinates.append([float(value) for value in split_line[1:4]])
            atoms.append(str(split_line[0]))
        except Exception:
            print(f'file {filename} not a proper xyz-file')
    return atoms, coordinates


def read_zmat(filename: str) -> Sequence[Union[str, int, float]]:
    """Read molecular structure from zmat file.

    Parameters
    ----------
    filename : str
        filename for molecular structure

    Returns
    -------
    :obj:`Sequence[Union[str, int, float]]`
        zmat

    """
    #if not os.path.isabs(filename):
    #    os.path.join(self.settings.work_dir, filename)
    with open(filename, 'r') as myfile:
        lines = myfile.read().split('\n')
        lines2 = [x for x in lines if x != []]
        if not isinstance(lines[0][0], str):
            print(f'file {filename} not a proper zmat-file')
    zmat = []
    for line in lines2:
        if line.split() == []:
            continue
        try:
            zmat.append(line.split())
        except Exception:
            print(f'file {filename} not a proper zmat-file')
    return zmat


def write_xyz(
    filename: str,
    atoms: List[str],
    coordinates: Union[List[float], np.ndarray],
) -> None:
    """Write atoms and coordinates to file in xyz-format.

    Parameters
    ----------
    filename : str
        filename for molecular structure
    atoms : list
        list of atoms
    coordinates : list
        coordinates

    """
    num_atoms = len(atoms)

    #if not os.path.isabs(filename):
    #    os.path.join(self.settings.work_dir, filename)
    myfile = open(filename, 'w')
    myfile.write(str(num_atoms))
    myfile.write('\n')
    myfile.write('hylleraas geometry\n')
    for i, atom in enumerate(atoms):
        x, y, z = map(float, (coordinates[i]))
        myfile.write('{}\t {}\t {}\t {} \n'.format(atom, x, y, z))
    myfile.close()


def write_zmat(filename: str, zmat: Sequence[Union[str, int, float]]) -> None:
    """Write zmat to file.

    Parameters
    ----------
    filename : str
        filename for molecular structure
    zmat : :obj:`equence[Union[str, int,float]]`
        zmat

    """
    import csv

    #if not os.path.isabs(filename):
    #    os.path.join(self.settings.work_dir, filename)

    with open(filename, 'w') as myfile:
        wr = csv.writer(myfile, delimiter=' ')
        wr.writerows(zmat)


def read_cub(filename: str, return_molecule=False):
    """Read field f(x,y,z) from Gaussian-style (ascii)-cube file.

    Parameters
    ----------
    filename : str
        filename
    return_molecule: bool, optional
        if to return atoms and coordinates of the molecule. Defaults to False.

    """
    # if not os.path.isabs(filename):
    #     os.path.join(os.getcwd(), filename)
    with open(filename, 'r') as cube_file:
        line = cube_file.readline()
        line = cube_file.readline()
        split_line = cube_file.readline().split()
        num_atoms = int(split_line[0])
        origin = np.array(split_line[1:]).astype(np.float64)
        split_line = cube_file.readline().split()
        n1 = int(split_line[0])
        ax1 = np.array(split_line[1:]).astype(np.float64)
        split_line = cube_file.readline().split()
        n2 = int(split_line[0])
        ax2 = np.array(split_line[1:]).astype(np.float64)
        split_line = cube_file.readline().split()
        n3 = int(split_line[0])
        ax3 = np.array(split_line[1:]).astype(np.float64)
        n_tot = n1 * n2 * n3
        coordinates = []
        atoms = []
        for i in range(0, num_atoms):
            split_line = cube_file.readline().split()
            atoms.append(periodictable.to_E(split_line[0]))
            coordinates.append(np.array(split_line[2:]).astype(np.float64))
        val = np.zeros(n_tot)
        i = 0
        for line in cube_file:
            for vval in line.split():
                val[i] = float(vval)
                i += 1
        coord_min = origin
        coord_max = origin + n1*ax1 + n2*ax2 + n3*ax3
        dims = [n1, n2, n3]
        if return_molecule:
            return val, dims, coord_min, coord_max, atoms, coordinates
        else:
            return val, dims, coord_min, coord_max
