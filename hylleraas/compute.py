from __future__ import annotations

from typing import Any, Optional

from hyobj import MoleculeLike
# from hyset import ComputeSettings, create_compute_settings
from hyset import ComputeSettings

from .method import MethodLike

try:
    import hygo
    found_hygo = True
except Exception:  # pragma no cover
    found_hygo = False

IMPLEMENTED = {'geometry_optimization': hygo.geometry_optimization}


class Compute:
    """Hylleraas Compute class."""

    def __init__(self,
                 keyword: str,
                 method: type[MethodLike],
                 molecule: Optional[type[MoleculeLike]] = None,
                 options: Optional[dict] = None,
                 compute_settings: Optional[ComputeSettings] = None) -> None:
        """Create Hylleraas Compute instance.

        Parameters
        ----------
        keyword : str
            keyword for computation
        molecule : :obj:`MoleculeLike`
            molecule instance
        method : :obj:`MethodLike`
            method instance
        options : dict
            options for compute method, e.g. hygo
        compute_settings: :obj:`ComputeSettings`
            compute settings from hyset, currently unsed

        """
        self.keyword = keyword
        self.molecule = molecule  # method.molecule if hasattr(method, 'molecule') else molecule  # type: ignore
        self.method = method
        self.options = options
        # self.compute_settings = compute_settings if compute_settings is not None else create_compute_settings()
        self.result = self.perform_computation(self.keyword, self.molecule, self.method, self.options)

    @classmethod
    def perform_computation(cls,
                            keyword: str,
                            molecule: type[MoleculeLike],
                            method: type[MethodLike],
                            options: Optional[dict] = None) -> Any:
        """Perform an actual computation.

        Parameters
        ----------
        keyword : str
            type of calculation (currently, only 'geometry_optimization') is supported
        molecule : :obj:`MoleculeLike`
            hylleraas Molecule instance
        method : :obj:`MethodLike`
            hylleraas Method instance
        options : dict, optional
            options for the computation

        """
        try:
            result = IMPLEMENTED[keyword](molecule=molecule, method=method, options=options)
        except Exception:
            raise Exception(f'could not perform computation {keyword}')
        return result
