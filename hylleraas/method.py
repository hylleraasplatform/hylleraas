from __future__ import annotations

import inspect
import os
import warnings
from abc import ABC, abstractmethod
from ast import literal_eval
from typing import Any, Callable, Optional, Tuple, Union

# from . import interfaces
import hyif
from hyobj import Molecule, MoleculeLike
from hyset import ComputeSettings, create_compute_settings

from .errors import InputError
from .utils import get_val_from_arglist, key_in_arglist

COMMENT_SYMBOLS = ['#', '!']
DELIMITER_SYMBOLS = ['=', ':', ',']


class MethodLike(ABC):
    """Base class for method-like computable objects on the Hylleraas Software Platform."""

    @property
    @abstractmethod
    def version(self) -> str:
        """Set the version."""
        pass  # pragma no cover


class Method(MethodLike):
    """Hylleraas Method class."""

    def __new__(cls,
                method_input=None,
                molecule=None,
                program=None,
                interface=None,
                compute_settings=None,
                **kwargs):
        """Allow Method(Method) calls."""
        if isinstance(method_input, MethodLike) or isinstance(method_input, Method):
            return method_input
        else:
            return object.__new__(cls)

    def __init__(self,
                 method_input: Optional[Any] = None,
                 program: Optional[str] = None,
                 interface: Optional[Union[hyif.HylleraasInterface, Callable]] = None,
                 compute_settings: Optional[ComputeSettings] = None,
                 **kwargs) -> None:
        """Create Hylleraas Method instance.

        Parameters
        ----------
        method_input : :obj:`Any`, optuional
            method input (str, list, dict, or class instance), defaults to {}
            might include a molecule definition for methods which are explicitely
            molecule-dependent. Note that this molecule will *not* be used in any
            computation
        program : str, optional
            program name, defaults to None
        interface : :obj:`HylleraasInterface`
            explicit interface input, defaults to None
        compute_settings : :obj:`ComputeSettings
            compute settings object, defaults to 'local'


        Note
        ----
        Either an explicit *program* or *interface* input is required.
        The program name might also be provided as a keyword inside *input*

        """
        method_input = method_input if method_input is not None else {}
        if isinstance(method_input, MethodLike) or isinstance(method_input, Method):
            self = method_input  # type: ignore
            return
        elif isinstance(method_input, dict):
            method_input.update(kwargs)

        self.program: str = self._set_program(method_input, program, interface)

        self.compute_settings = compute_settings if compute_settings is not None else create_compute_settings()

        self.interface = self._set_interface(interface)
        self.get_input_method: Callable = self.get_method_constructor(method_input)
        self.method: dict = self.get_input_method(method_input)
        # converted = dict2obj(self, self.method)

        # self.environment = self._set_environment(self.method)
        self.molecule: MoleculeLike = self._set_molecule(self.method)
        self.bind_interface(self.interface, self.compute_settings)

    def __repr__(self) -> str:  # pragma no cover
        """Dataclass-like representation."""
        keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
        return '{}({})'.format(type(self).__name__, ', '.join(keywords))

    def __add__(self, args: Any) -> object:
        """Add two (or more) methods from Method instance.

        Note:
        ----
            assume a = b + c , this code will *destroy* b

        """
        _method = Method.__new__(self, self)  # type: ignore
        arglist = [args] if not isinstance(args, list) else args
        if not all(callable(func) for func in arglist):
            raise Exception('can only add functions to class instance')

        for arg in arglist:
            setattr(_method, arg.__name__, arg)

        return Method.__new__(self, _method)  # type: ignore

    def __sub__(self, args: Any) -> MethodLike:
        """Subtract two (or more) methods from Method instance."""
        _method = Method.__new__(self, self)  # type: ignore
        arglist = [args] if not isinstance(args, list) else args
        if not all(callable(func) for func in arglist):
            raise Exception('can only add functions to class instance')

        for arg in arglist:
            if hasattr(_method, arg.__name__):
                delattr(_method, arg.__name__)

        return Method.__new__(self, _method)  # type: ignore

    def __contains__(self, args: Any) -> bool:
        """Check if (list of) method(s) is contained in class instance."""
        arglist = [args] if not isinstance(args, list) else args

        for arg in arglist:
            name = arg.__name__ if callable(arg) else arg
            if not hasattr(self, name):
                return False
        return True

    def __eq__(self, other: object) -> bool:
        """Check if two methods are identical."""
        if not isinstance(other, MethodLike):
            return False
        _method_list_str = [method for method in dir(other) if method.startswith('_') is False]
        return self.__contains__(_method_list_str)

    def _set_program(self, input, program, interface) -> str:
        """Find program name in method input."""
        if interface is not None:
            return 'user-defined'
        else:
            if program is not None:
                return program
            else:
                if key_in_arglist(input, look_for='program'):
                    return get_val_from_arglist(input, look_for='program')
                else:
                    raise InputError('Method input requires either program name or explicit interface input')

    def _set_interface(self, input_interface):
        """Set interface."""
        if input_interface is not None:
            if isinstance(input_interface, str):
                return InputError('Explicit interface input must a class instance')
            # if inspect.isclass(type(input_interface)):
            return input_interface
        else:
            if self.program in hyif.PROGRAMS:
                return getattr(hyif, hyif.PROGRAMS[self.program])
            else:
                try:
                    return getattr(hyif, self.program)
                except Exception:
                    return InputError(f'no interface to program {self.program} found')

    # def _set_environment(self, method: dict) -> Environments:
    #    """Set computational environment."""
    #    environment: Environments
    #    if 'environment' in method.keys():
    #        if isinstance(method['environment'], Environments):
    #            environment= method['environment']
    #        if isinstance(method['environment'], dict):
    #            run_dir = method['environment'].get('run_dir')
    #            bin_dir = method['environment'].get('bin_dir')
    #            scr_dir = method['environment'].get('scr_dir')
    #            env = method['environment'].get('env')
    #            environment = create_environment(run_dir=run_dir, bin_dir=bin_dir, scr_dir=scr_dir, env=env)
    #    else:
    #        environment = create_environment()

    #    return environment

    def _set_molecule(self, method: dict) -> MoleculeLike:
        """Set molecule."""
        # if input_molecule is not None:
        #     if isinstance(input_molecule, Molecule):
        #         return input_molecule
        #     else:
        #         return Molecule(input_molecule)
        # else:
        # if 'molecule' in method:
        #     return Molecule(method['molecule'])
        # # else:
        #     try:
        #         reader = getattr(self.interface, 'get_input_molecule')
        #     except Exception:
        #         return None
        #         # raise InputError('could not find molecule object')
        #     else:
        #         return Molecule(reader(self.filename))
        # return None

        mol = method.get('molecule', None)
        if mol is not None:
            mol = Molecule(mol)
        return mol

    def get_method_constructor(self, input: Any) -> Callable:
        """Select correct method constructor by analysing input.

        Parameters
        ----------
        input : :obj:`Any`
            method input (str, list, dict, or class instance)

        Returns
        -------
        :obj:`Callable`
            method that constructs a Hylleraas method from input

        Note
        ----
        We tried to balance user-friendliness with SOLID programming.
        Therefore, most of the parsing is condensed in this routine.

        """
        self.filename = None
        if input is None:
            return MethodReaders.get_empty_method
        elif isinstance(input, str):
            # case a : input is a filename
            if os.path.exists(input) or os.path.exists(os.path.join(os.getcwd(), input)):
                self.filename = input
                # extract file extension if not given, useful for finding delimiters
                # self.filetype = os.path.splitext(self.filename)[1][1:]
                reader = None

                try:
                    reader = getattr(self.interface, 'get_input_method')
                except Exception:
                    reader = MethodReaders.GenericMethodFileReader.get_input_method
                finally:
                    return reader
            # case b : explicit method input
            else:
                return MethodReaders.get_method_from_string
        # case ii : list
        elif isinstance(input, list):
            raise NotImplementedError('List input not supported, use dict or str instead')
        # case iii : dictionary
        elif isinstance(input, dict):
            return MethodReaders.idlereader
        else:
            # case iv : class
            if inspect.isclass(type(input)):
                return MethodReaders.get_method_from_class
        return None  # pragma no cover

    def bind_interface(self, interface: Callable, compute_settings: ComputeSettings) -> None:
        """Bind interface methods to current class instance."""
        if self.molecule is not None:
            try:
                _instance = interface(self.method, self.molecule, self.compute_settings)
            except Exception:
                _instance = interface(self.method, self.compute_settings)
        else:
            _instance = interface(self.method, self.compute_settings)
        # bind attributes
        self.__dict__.update(_instance.__dict__)
        # bind classmethods
        _method_list_str = [method for method in dir(_instance) if method.startswith('_') is False]
        _method_list = [getattr(_instance, method) for method in _method_list_str]
        for method in _method_list:
            if not callable(method):
                continue
            name = method.fget.__name__ if isinstance(method, property) else method.__name__
            try:
                setattr(self, name, method)
            except Exception:
                raise Exception(f'Could not bind method {method} with name {name}')

    def restart(self, input: Any) -> None:
        """Perform restart. If there is not interface, performs generic restart.

        Parameters
        ----------
        input : Any
            input, e.g. output of geometry optimization

        """
        # we have self.program
        # check if there is a restart option

        if hasattr(self.interface, 'restart'):
            _restart = getattr(self.interface, 'restart')
        else:
            _restart = self.generic_restart

        try:
            _restart(input)
            # if hasattr(self, '_method') and hasattr(self, '_molecule'):
            #     self.initialize(self._method, self._molecule, program=self.program)
            # else:
            #     _method_list_str = [method for method in dir(self) if method.startswith('__') is False]
            #     _method_list = [getattr(self, method) for method in _method_list_str]
            #     self.initialize(_method_list, program=self.program)
        except Exception:
            warnings.warn(f'could not restart Method class using input {input}')
        self.bind_interface(self.interface, self.compute_settings)

    def generic_restart(self, *args) -> None:
        """Perform generic restart."""
        pass

    @property
    def version(self):
        """Return version."""
        return '0.1b'


# class Input(MethodLike):
#     """Main Hylleraas method class. Converts any input into dictionary."""

#     def __init__(self, method_input: Any, options: Optional[dict] = None, **kwargs):

#         self.options = options

#         if self.options is not None:
#             if 'filetype' in self.options:
#                 self.filetype = self.options['filetype']
#             else:
#                 self.filetype = None
#         else:
#             self.filetype = None

#         if key_in_arglist(method_input, options, look_for = 'program', **kwargs):
#             self.program = get_val_from_arglist(method_input, options, look_for = 'program', **kwargs)

#         # self.filetype = self.options['filetype'] if 'filetype' in self.options else None

#         self.get_method = self.get_method_constructor(method_input)

#         self._method: dict = self.get_method(method_input)

#         self._method['_type'] = self.set_method_type(self._method)

#         converted = self.dict2obj(self._method)
#         if not converted:  # pragma no cover : only for mypy
#             raise GenericError('could not create class attributes')

# for future implementation : allow dynamical updating
# def __set_name__(self, owner, name):
#      self.name = name
# def __get__(self, obj, type=None) -> object:
#     return obj.__dict__.get(self.name) or 0
# def __set__(self, obj, value) -> None:
#     print('entering setter')
#     obj.__dict__[self.name] = value
#     obj._method.update({self.name :value})
# def __delete__(self, obj) -> None:
#     del obj.__dict__[self.name]

# def __repr__(self) -> str:  # pragma no cover
#     """Dataclass-like representation."""
#     keywords = [f'{key}={value!r}' for key, value in self.__dict__.items()]
#     return '{}({})'.format(type(self).__name__, ', '.join(keywords))

# def __add__(self, methods: Any) -> Input:
#     """Add two (or more) Input instances.

#     Parameters
#     ----------
#     methods : :obj:`hylleraas.Input` or :obj:`List[hylleraas.v]`
#         Input (fragments) to be merged

#     Returns
#     -------
#     :obj:`hylleraas.Input`
#         new hyllerraas.Input object

#     """
#     new_method: dict = copy.deepcopy(self._method)
#     new_options: dict = copy.deepcopy(self.options) if self.options is not None else {}

#     method_list = [methods] if not isinstance(methods, list) else methods
#     for method in method_list:
#         new_options.update(method.options)
#         for key, val in method._method.items():
#             # if key in ['options', 'get_method', '_method']:
#             #     del new_method[key]
#             #     continue
#             if key not in new_method:
#                 new_method[key] = val
#             else:
#                 warnings.warn(f'method already contains key {key}')
#     return Input(method_input=new_method, options=new_options)

# def __sub__(self, methods: Any) -> Input:
#     """Subtract two (or more) Input instances.

#     Parameters
#     ----------
#     methods : :obj:`hylleraas.Input` or :obj:`List[hylleraas.Input]`
#         Input (fragments) to be merged

#     Returns
#     -------
#     :obj:`hylleraas.Input`
#         new hyllerraas.Input object

#     """
#     new_method: dict = copy.deepcopy(self._method)
#     new_options: dict = copy.deepcopy(self.options) if self.options is not None else {}

#     # for key in ['options', 'get_method', '_method']:
#     #     del new_method[key]

#     method_list = [methods] if not isinstance(methods, list) else methods
#     for method in method_list:
#         for key, val in method.options.items():
#             if key in new_options:
#                 del new_options[key]

#         for key, val in method._method.items():
#             # if key in ['options', 'get_method', '_method']:
#             #     continue
#             if key in new_method:
#                 del new_method[key]
#             else:
#                 warnings.warn(f'method does not contain key {key}')
#     return Input(method_input=new_method, options=new_options)

# def __eq__(self, method2: Any) -> bool:
#     """Check if two methods are equal.

#     Parameters
#     ----------
#     method2 : :obj:`hylleraas.Input`
#         method to be checked

#     Returns
#     -------
#     bool
#         if the methods are equal or not

#     """
#     return self._method == method2._method and self.options == method2.options

# @property
# def name(self) -> str:
#     """Set name attribute.

#     Returns
#     -------
#     str
#         class instance name

#     """
#     _name = self._method['name'] if 'name' in self._method else 'unknown'
#     return _name

# def dict2obj(self, data: dict) -> bool:
#     """Convert dict to class instance attributes.

#     Parameters
#     ----------
#     data : dict
#         input dictionary

#     """
#     if isinstance(data, dict):
#         for key in data:
#             if not isinstance(data[key], dict):
#                 self.__dict__.update({key: data[key]})
#             else:
#                 self.__dict__.update({key: self.dict2obj(data[key])})
#         return True
#     return False

# def set_method_type(self, method: dict) -> str:
#     """Set method type.

#     Parameters
#     ----------
#     method : dict
#         input dictionary

#     """
#     _name = method['program'] if 'program' in method else 'unknown'
#     if _name in interfaces.QC_PROGS:
#         return 'quantum_chemistry'
#     else:
#         return _name

# def get_method_constructor(self, input: Any) -> Callable:
#     """Select correct method constructor by analysing input.

#     Parameters
#     ----------
#     input : :obj:`Any`
#         method input (str, list, dict, or class instance)

#     Returns
#     -------
#     :obj:`Callable`
#         method that constructs a Hylleraas method from input

#     Note
#     ----
#     We tried to balance user-friendliness with SOLID programming.
#     Therefore, most of the parsing is condensed in this routine.

#     """
#     if input is None:
#         return MethodReaders.get_empty_method
#     elif isinstance(input, str):
#         # case a : input is a filename
#         if os.path.exists(input) or os.path.exists(os.path.join(os.getcwd(),input)):
#             self.filename = input

#             # extract file extension if not given, useful for finding delimiters
#             if self.filetype is None:
#                 self.filetype = os.path.splitext(self.filename)[1][1:]

#             if self.filetype in interfaces.EXTENSIONS:
#                 _compute = getattr(interfaces, interfaces.EXTENSIONS[self.filetype])
#                 try:
#                     reader = getattr(_compute, 'get_input_method')
#                 except Exception:
#                     raise InputError(f'could not find method get_input_method in {_compute}')
#                 else:
#                     return reader
#             elif self.filetype in ['inp']:
#                 _compute = getattr(interfaces, interfaces.PROGRAMS[self.program])
#                 try:
#                     reader = getattr(_compute, 'get_input_method')
#                 except Exception:
#                     raise InputError(f'could not find method get_input_method in {_compute}')
#                 else:
#                     return reader
#             else:
#                 try:
#                     reader = MethodReaders.GenericMethodFileReader
#                 except Exception:
#                     raise NotImplementedError(
#                         f'No reader for filetype {self.filetype} of file {self.filename} not implemented.'
#                     )
#                 else:
#                     return reader.get_method_from_file
#         # case b : explicit method input
#         else:
#             return MethodReaders.get_method_from_string
#     # case ii : list
#     elif isinstance(input, list):
#         raise NotImplementedError('List input not supported, use dict or str instead')
#     # case iii : dictionary
#     elif isinstance(input, dict):
#         return MethodReaders.idlereader
#     else:
#         # case iv : class
#         if inspect.isclass(type(input)):
#             return MethodReaders.get_method_from_class

#     # case v : not implemented
#     raise InputError(
#         f'Could not generate method constructor from input {input}')

# class MethodAttr:
#     def __init__(self, data:dict):
#         self.convert(data)

#     def convert(self, data:dict):
#         if not isinstance(data, dict):
#             return
#         for key in data:
#             if not isinstance(data[key], dict):
#                 self.__dict__.update({key: data[key]})
#             else:
#                 self.__dict__.update({ key: MethodAttr(data[key])})


class MethodReaders:
    """Routines for reading method informations from various input formats."""

    @classmethod
    def idlereader(cls, input: dict) -> dict:
        """Do nothing.

        Parameters
        ----------
        input : dict
            input dictionary

        Returns
        -------
        dict:
            input dictionary

        """
        return input

    @classmethod
    def get_empty_method(cls, input: Any) -> dict:
        """Return empty dict.

        Parameters
        ----------
        input : str
            input string

        Returns
        -------
        dict:
            empty dictionary

        """
        return {}

    @classmethod
    def get_method_from_class(cls, instance: Any) -> dict:
        """Convert class instance input to dict.

        Parameters
        ----------
        instance : :obj:`class instance`
            input class instance

        Returns
        -------
        dict:
            output dictionary

        """
        # support for self
        if hasattr(instance, 'method'):
            return instance.method
        else:
            return instance.__dict__

    @classmethod
    def get_method_from_string(cls, input: str) -> dict:
        """Convert string input to dict.

        Parameters
        ----------
        input : str
            input string

        Returns
        -------
        dict:
            output dictionary

        """
        method_dict: dict = {}
        lines: list = list(input.strip().split('\n'))
        for line in lines:
            # print(line)
            if line[0] in COMMENT_SYMBOLS:
                continue
            key, val = cls.read_key_val_from_line(line)
            if key == '0':
                continue
            method_dict[key] = val
        return method_dict

    @classmethod
    def read_key_val_from_line(cls, line: str) -> Tuple[str, Any]:
        """Read key and value from string.

        Parameters
        ----------
        line : str
            input line

        Returns
        -------
        :obj:`Tuple[str,Any]`
            Pair of key and value

        """
        # find delimiter
        for delim in DELIMITER_SYMBOLS:
            # take the first one found
            if delim in line:
                key, val = line.split(delim)
                return key.strip(), cls.adjust_type(val.strip())
        return ('0', 0)  # pragma no cover

    @classmethod
    def adjust_type(cls, input: str) -> Any:
        """Adjust type of arbitrary input.

        Parameters
        ----------
        input : str
            input string

        Returns
        -------
        :obj:`Any`
            actual type of input (float, input, str, ...)

        """
        try:
            res = literal_eval(input.strip())
            return res
        # is string
        except Exception:
            return input

    class MethodFileReader(ABC):
        """Base class for methods that read method information from file."""

        @abstractmethod
        def get_input_method(self, filename: str):
            """Construct method from file."""
            pass  # pragma no cover

    class GenericMethodFileReader(MethodFileReader):
        """Generic File Reader."""

        @classmethod
        def get_input_method(cls, filename: str) -> dict:
            """Read method to string from file and convert to dict.

            Paramters
            ---------
            filename : str
                filename

            Returns
            -------
            dict
                method dictionary

            """
            with open(filename, 'r') as myfile:
                lines = myfile.read()
            try:
                return MethodReaders.get_method_from_string(lines)
            except Exception:
                raise InputError(f'could not read input from file {filename}')


# OLD

# class Method_old:
#     """Hylleraas method class

#     Args:
#             name : Name of the method (currently "HF", "GHF" (LONDON only) or "DFT")

#     Optional:
#             program[lsdalton] : name of program for calculation (currently "lsdalton")
#             basis["pcseg-1"] : name of basis set from daltonproject basis catalogue
#             settings[None] : hylleraas Settings instance
#             filename[None] : file name for storing inputs/outpus
#             program_parameter: Optional[None] = dictionary containing parameters for Method.program
#                 Currently, only the following keys are recognized for LONDON:
#                 {"magnetic_field": [0,0,0],
#                  "gauge_origin": [0,0,0],
#                  "use_london_orbitals": True}

#     """
#     def __new__(
#         cls,
#         name: str,
#         program: Optional[str] = 'lsdalton',
#         basis: Optional[str] = 'pcseg-1',
#         filename: Optional[str] = None,
#         settings: Optional[Settings] = None,
#         program_parameter: Optional[Dict] = None,
#     ):
#         if settings is None:
#             settings = Settings()

#         if program.lower() == 'dalton':
#             self = Dalton(name, basis, settings=settings)
#         elif program.lower() == 'lsdalton':
#             self = LSDalton(name, basis, settings=settings)
#         elif program.lower() == 'london':
#             self = London(name=name, basis=basis, filename=filename,
# settings=settings, london_params=program_parameter)
#         else:
#             raise Exception(f'program {program} not available')
#         return self

# will be moved to compute class
# class Dalton(HylleraasMethodLike):
#     """hylleraas Method class for Dalton"""
#     def __init__(self, name: str, basis: str, settings: Settings) -> None:
#         self.settings = settings
#         self.name = name
#         self.basis = basis
#         self.dp_Basis = dp.Basis(basis=self.basis)
#         self.dp_ComputeSettings = dp.ComputeSettings(
#             work_dir=settings.work_dir, scratch_dir=settings.scratch_dir)

#         if 'DFT' in str(self.name).upper():
#             dft_split = self.name.split('/')
#             if len(dft_split) == 1:
#                 self.functional = 'B3LYP'
#             else:
#                 self.functional = dft_split[1].upper()

#             self.dp_Method = dp.QCMethod(qc_method='DFT',
#                                          xc_functional=self.functional)
#         elif 'HF' in str(self.name).upper():
#             self.dp_Method = dp.QCMethod(qc_method='HF')
#         else:
#             raise Exception(
#                 f'Method {self.name} not implemented, please use DFT or HF')

#     @property
#     def program(self) -> str:
#         return 'dalton'

#     @property
#     def has_energy(self) -> bool:
#         return True

#     @property
#     def has_gradient(self) -> bool:
#         return True

#     @property
#     def gradient_type(self) -> str:
#         return 'analytical'

# class LSDalton(HylleraasMethodLike):
#     """hylleraas Method class for LSDalton"""
#     def __init__(self, name: str, basis: str, settings: Settings) -> None:
#         self.settings = settings
#         self.name = name
#         self.basis = basis
#         self.dp_Basis = dp.Basis(basis=self.basis)
#         self.dp_ComputeSettings = dp.ComputeSettings(
#             work_dir=settings.work_dir, scratch_dir=settings.scratch_dir)

#         if 'DFT' in str(self.name).upper():
#             dft_split = self.name.split('/')
#             if len(dft_split) == 1:
#                 self.functional = 'B3LYP'
#             else:
#                 self.functional = dft_split[1].upper()

#             self.dp_Method = dp.QCMethod(qc_method='DFT',
#                                          xc_functional=self.functional)
#         elif 'HF' in str(self.name).upper():
#             self.dp_Method = dp.QCMethod(qc_method='HF')
#         else:
#             raise Exception(
#                 f'Method {self.name} not implemented, please use DFT or HF')

#     @property
#     def program(self) -> str:
#         return 'lsdalton'

#     @property
#     def has_energy(self) -> bool:
#         return True

#     @property
#     def has_gradient(self) -> bool:
#         return True

#     @property
#     def gradient_type(self) -> str:
#         return 'analytical'

# class London(HylleraasMethodLike):
#     """hylleraas Method class for London"""
#     def __init__(self, name: str, basis: str, filename: str, settings: Settings, london_params: Dict) -> None:
#         self.settings = settings
#         self.name = name
#         self.basis = basis
#         if 'magnetic_field' in london_params:
#             self.magnetic_field = list(london_params['magnetic_field'])
#         else :
#             self.magnetic_field= [0,0,0]

#         if 'gauge_origin' in london_params:
#             self.gauge_origin = list(london_params['gauge_origin'])
#         else :
#             self.gauge_origin= [0,0,0]

#         if 'use_london_orbitals' in london_params:
#             self.use_london_orbitals = london_params['use_london_orbitals']
#         else :
#             self.use_london_orbitals = True

#         self.filename = filename
#         self.settings = settings

#         if 'HF' not in str(self.name).upper():
#             raise Exception('London works currently only with HF or GHF')

#     @property
#     def program(self) -> str:
#         return 'london'

#     @property
#     def has_energy(self) -> bool:
#         return True
#     @property
#     def has_gradient(self) -> bool:
#         return True
#     @property
#     def gradient_type(self) -> str:
#         return 'analytical'
#         #return 'H-F forces'

# if __name__ == '__main__':

#     mymol = Molecule('H 0 0 0')
#     class myif:
#         pass

#     def myfunc1():
#         pass
#     test = Method(None, molecule = mymol,interface=myif)
#     print(test)
#     test = Method('./examples/method_input_generic.txt', molecule = mymol, interface=myif)
#     print(test)
#     print(test.get_input_method('./examples/method_input_generic.txt'))
#     test = Method(None, molecule = mymol, program='dalton')
#     print(test)
#     print('\nTESTING LONDON')
#     test = Method('./examples/method_input_london.inp', program='london')
#     print(test,'\n\n')
#     print(test.get_input_method('./examples/method_input_london.inp'))

#     test2 = Method(test)
#     print('\n','OLDINSTAC', test, '\nTHE NEW INSTACE',test2, '\nEND')
#     test = Method(None, molecule = mymol,interface=myif)
#     print('\nOLD\n',test)
#     test2 = test + myfunc1
#     print('\nNEW',test2)
#     print(myfunc1 in test2)

#     test3 = test2 - myfunc1
#     print('\nNEWNEW',test3)
#     print(test==test3)
#     print('this is test2', test2)
#     print('this is myfucn1', myfunc1)
#     print(myfunc1 in test2)
