class GenericError(Exception):
    """Generic Error class."""

    def __init__(self, message: str) -> None:
        self.message = message
        super().__init__(message)


class InputError(Exception):
    """Input Error class."""

    def __init__(self, message: str) -> None:
        self.message = message
        super().__init__(message)


class NotFoundError(Exception):
    """Not found error class."""

    pass
