from typing import List, Optional, Union

import numpy as np
from numpy.typing import NDArray


class Processing:
    """Hylleraas data processing class."""

    @staticmethod
    def filter_scalar_field(
        coord: Union[List, np.ndarray],
        val: Union[List, np.ndarray],
        isovalue: float,
        isorange: Optional[float] = None,
    ) -> NDArray:
        """Read scalar field x,y,z,f(x,y,z) ascii-file.

        Parameters
        ----------
        coord : list
            xyz coordinates
        val : list
            f(x,y,z) field values
        isovalue : float
            isovalue
        isorange : float, optional
            sets the range around isovalue, defaults to None.

        Returns
        -------
        list
            filtered coordinates

        """
        if isorange is None:
            isorange = 0.2 * abs(isovalue)

        minval = isovalue - isorange
        maxval = isovalue + isorange
        if len(coord) != len(val):
            print(len(coord), len(val))
            raise Exception(' scalar field dimension discrepancy')
        rm_ind = []
        for i in range(0, len(val)):
            if val[i] < minval or val[i] > maxval:
                rm_ind.append(i)

        return np.delete(coord, rm_ind, axis=0)

    @staticmethod
    def interpolate_field(data, npoints, bounds, npointsi, boundsi):
        """Compute an interpolated field f(xi,zi,yi) from input field f(x,y,z).

        Note
        ----
        Requires regular grids.

        Parameters
        ----------
        data : array
            input field values
        npoints : list
            number of points in x,y,z direction
        bounds : list
            bounds of the input grid (xmin, ymin, zmin, xmax, ymax, zmax)
        npointsi : list
            number of points in xi,yi,zi direction
        boundsi : list
            bounds of the output grid (ximin, yimin, zimin, ximax, yimax, zimax)

        """
        from scipy.interpolate import interpn

        data = np.array(data).ravel()
        bounds = np.array(bounds).ravel()
        boundsi = np.array(boundsi).ravel()

        x = np.linspace(bounds[0], bounds[3], npoints[0])
        y = np.linspace(bounds[1], bounds[4], npoints[1])
        z = np.linspace(bounds[2], bounds[5], npoints[2])

        xi = np.linspace(boundsi[0], boundsi[3], npointsi[0])
        yi = np.linspace(boundsi[1], boundsi[4], npointsi[1])
        zi = np.linspace(boundsi[2], boundsi[5], npointsi[2])

        mesh = np.array(np.meshgrid(xi, yi, zi))
        # points= np.rollaxis(mesh, 0, 4)
        points = np.moveaxis(mesh, 0, 3)
        points = points.reshape((mesh.size // 3, 3))

        data_new = interpn((x, y, z),
                           data.reshape(npoints),
                           points,
                           method='linear',
                           bounds_error=False,
                           fill_value=0.0)
        data_out = np.zeros(npointsi)
        ii = 0
        # note the order
        for j in range(0, npointsi[1]):
            for i in range(0, npointsi[0]):
                for k in range(0, npointsi[2]):
                    data_out[i][j][k] = data_new[ii]
                    ii += 1

        del points, data_new
        return data_out.ravel()
