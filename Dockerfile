# Example usage:
#
# Build Jupyter image with
#
#    docker build --build-arg branch="new_structure" -t hylleraas/jupyterhub:X -t hylleraas/jupyterhub:latest .
#
# then start the Docker image with
#
#    docker run -v "$PWD:/tmp" -p 8888:8888 hylleraas/jupyterhub:latest
#
# and open browser as specified in the printed instructions
#
FROM daltonproject/daltonproject
ARG branch="master"

WORKDIR /

#RUN git clone -b ${branch} --depth 1 --single-branch https://gitlab.com/hylleraasplatform/hylleraas.git
WORKDIR /hylleraas
COPY . .
RUN pip install -r requirements.txt
RUN pip install .
ENV PYTHONPATH /hylleraas:$PYTHONPATH
# workaround
RUN pip install nglview && jupyter-nbextension enable nglview --py --sys-prefix
RUN pip install ipyvolume && jupyter-nbextension enable ipyvolume --py --sys-prefix
RUN jupyter-nbextension enable widgetsnbextension --py --sys-prefix

RUN apt-get --yes -qq update && \
    DEBIAN_FRONTEND="noninteractive" \
    apt-get --yes -qq --no-install-recommends install python2.7 && \
    apt-get --yes -qq install blender &&\
    apt-get --yes -qq clean && \
    rm -rf /var/lib/apt/lists/*

RUN ln -nfs /usr/bin/python2.7 /usr/bin/python2


WORKDIR /tmp

# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]

ENV TERM vt100
ENV SHELL /bin/bash

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]

