# Hylleraas Software Platform

The Hylleraas Software Platform is an ongoing project aimed at seamlessly linking the many software development and computational activities at the [Hylleraas Centre](https://www.mn.uio.no/hylleraas/english/), in order to pave the way for advanced multi-scale simulations spanning time scales from atto- to milliseconds and length scales from single atoms to billions of atoms.

The project is open source, and free to use by anyone. If you have ideas or corrections others can benefit from, you are also welcome to contribute to the Platform (we currently have no contribution guidelines, but will communicate these on request).

# Documentation

The documentation can be found [here](https://hylleraas.readthedocs.io).


# Installation instructions

First, do a git clone
```
git clone https://gitlab.com/hylleraasplatform/hylleraas.git
```
From inside the folder, run
```
pip install .
```

# Structure of the Hylleraas Software Platform

![Structure of the HSP](hsp_schematics.png)
