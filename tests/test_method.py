import os

import pytest

from hylleraas import Molecule
from hylleraas.method import Method

mymol = Molecule('H 0 0 0')

method_string = """# this is a comment
    this is = {"test": "testdict"}
    key0 = -4
    key1 = 1.0
    key2 = 15.1
    key3 = [ 3,  5,4.0]
    key4 = stryy
    """


class Myif:
    """Dummy class."""

    def __init__(self, method, compute_settings):
        """Init class."""
        self.method = {'mytest': -2}

    def func0(self):
        """Return 0."""
        return 0


def myfunc1():
    """Return 1."""
    return 1


def myfunc2():
    """Return 2."""
    return 2


@pytest.fixture(scope='session', params=['method_input_generic.txt'])
def get_method_input_file(request):
    """Fixture returning molecule filename."""
    yield os.path.join(pytest.FIXTURE_DIR, request.param)


# should be tested in the individual interface
# @pytest.fixture(scope='session', params=['method_input_london.inp'])
# def get_method_input_file_london(request):
#    """Fixture returning molecule filename."""
#    yield os.path.join(pytest.FIXTURE_DIR, request.param)


def test_method_input(get_method_input_file):
    """Test Method class."""
    mymethod = Method(None, interface=Myif)
    assert mymethod.func0() == 0
    mymethod = Method(get_method_input_file, interface=Myif)
    assert mymethod.method['mytest'] == -2
    mymethod = Method(program='dalton')
    assert hasattr(mymethod, 'restart')
    assert mymethod.program == 'dalton'
    mymethod2 = Method({'program': 'dalton'})
    assert mymethod == mymethod2
    # mymethod = Method({'program': 'LSDalton'})
    # mymethod = Method(get_method_input_file_london, program='london')

    mymethod2 = Method(mymethod)
    assert mymethod == mymethod2

    mymethod3 = Method({'mykey': 'myval', 'qcmethod': 'HF', 'check_version': False}, program='xtb')
    mymethod4 = Method(mykey='myval', qcmethod='HF', check_version=False, program='xtb')
    mymethod5 = Method({'mykey': 'myval', 'qcmethod': 'HF', 'check_version': False, 'program': 'xtb'})
    assert mymethod3 == mymethod4
    assert mymethod3 == mymethod5

    mymethod3 = mymethod2 + myfunc1
    assert myfunc1 in mymethod3
    mymethod1 = mymethod3 - myfunc1
    assert mymethod1 == mymethod2
    assert mymethod != 1.0
    assert myfunc1 not in mymethod1

    mymethod = mymethod + [myfunc1, myfunc2]
    assert [myfunc1, myfunc2] in mymethod

    # mymethod = Method({},program='dalton')
    # mymethod = Method(None, program='dalton')
    # mymethod = Method({'molecule': mymol}, program='dalton')

    mymethod = Method(method_string, interface=Myif)

    # mymethod = Method(method_string, mymol, program='london')
    # mymethod = Method({'key1': -1.0, 'program': 'london', 'molecule': mymol})
    # mymethod = Method(Myif, mymol, program='london')
    # myclass = Myif(mymol, {})
    # mymethod = Method(myclass, mymol, program='london')
    # assert mymethod.molecule == mymol
    with pytest.raises(Exception):
        Method({})
    with pytest.raises(Exception):
        mymethod3 = mymethod1 - 1.0
    with pytest.raises(Exception):
        mymethod3 = mymethod1 + 1.0
    with pytest.raises(Exception):
        Method(None, interface='Myif')
    with pytest.raises(Exception):
        Method(None, program='DALTON')


# test2 = Method(test)
# print('\n','OLDINSTAC', test, '\nTHE NEW INSTACE',test2, '\nEND')
# test = Method(None, molecule = mymol,interface=myif)
# print('\nOLD\n',test)
# test2 = test + myfunc1
# print('\nNEW',test2)
# print(myfunc1 in test2)

# test3 = test2 - myfunc1
# print('\nNEWNEW',test3)
# print(test==test3)
# print('this is test2', test2)
# print('this is myfucn1', myfunc1)
# print(myfunc1 in test2)
# method_string1 = """# this is a comment
#     this is = {"test": "testdict"}
#     key0 = -4
#     key1 = 1.0
#     key2 = 15.1
#     key3 = [ 3,  5,4.0]
#     key4 = stryy
#     """
# mymethod = Method(method_string1)
# assert mymethod.key4 == 'stryy'
# mymethod2 = Method({'input2': 'tst'})
# mymethod3 = mymethod + mymethod2
# mymethod4 = mymethod3 - mymethod2
# assert mymethod4 == mymethod
# mymethod = mymethod3 + mymethod3
# assert mymethod == mymethod3
# assert mymethod.name == 'unknown'
# mymethod = Method(None)
# mymethod= Method(os.path.join(pytest.FIXTURE_DIR,'dummy.method'))
# assert mymethod.test == 245
# #     print('RESULT 1:', mymethod, type(mymethod))
# #     mymethod2 = Method('mybasis : test2')
# #     print('RESULT 2:', mymethod2)
# #     mymethod3 = Method(mymethod2)
# #     print('RESULT 3:', mymethod3)
# #     mymethod4 = Method(None)
# #     print('RESULT 4:', mymethod4)
# #     mymethod5 = mymethod + mymethod2
# #     print('RESULT 5:', mymethod5)
# #     mymethod6 = mymethod5 - mymethod
# #     print('RESULT 6:', mymethod6)

# with pytest.raises(Exception):
#     mymethod = Method(['a', 'b'])
# test = MethodReaders.read_key_val_from_line('t 3 ')
# assert test[0] == '0'
# class test:
#     def __init__(self):
#         self.key=1
# mytest = test()
# mymethod = Method(mytest)
# assert mymethod.key == 1
