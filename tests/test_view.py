import py3Dmol

from hylleraas import Molecule
from hylleraas.utils import view_molecule


def test_molecule_view():
    """Test view functionality of Hylleraas molecules."""
    caffeine: Molecule = Molecule('CN1C=NC2=C1C(=O)N(C(=O)N2C)C')
    view_obj: py3Dmol.view = view_molecule(caffeine)
    assert isinstance(view_obj, py3Dmol.view)
