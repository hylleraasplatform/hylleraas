import requests

# List of repositories with their corresponding package names and branch names
repositories = [
    ('geometry_optimization', 'hygo', 'main'),
    ('hylleraas-interfaces', 'hyif[full]', 'main'),
    ('hylleraas-tools', 'hytools', 'main'),
    ('hyset', 'hyset[full]', 'main'),
    ('hyobj', 'hyobj', 'main'),
    ('hyao', 'hyao', 'main'),
    ('hyrp', 'hyrp', 'main'),
    ('hyal', 'hyal', 'main'),
    ('hydb', 'hydb', 'main'),
]


def get_latest_commit_hash(repo_path, branch_name='main'):
    """Fetch the latest commit hash for a given GitLab repository path and branch."""
    url = f'https://gitlab.com/api/v4/projects/hylleraasplatform%2F{repo_path}/repository/commits?ref_name={branch_name}&per_page=1'  # noqa: E501
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()[0]['id']
    else:
        print(f'Failed to fetch commit for {repo_path} on branch {branch_name}: HTTP {response.status_code}')  # noqa: E501
        return None


def main():
    """Print the latest commit hashes for the repositories in HSP."""
    for repo_path, package_name, branch_name in repositories:
        commit_hash = get_latest_commit_hash(repo_path, branch_name)
        if commit_hash:
            print(f'{package_name} @ git+https://gitlab.com/hylleraasplatform/{repo_path}.git@{commit_hash}')  # noqa: E501


if __name__ == '__main__':
    main()
